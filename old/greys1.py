import xlwings as xw
import mysqlx
from datetime import datetime, date, timedelta
import time
import traceback
import json
import os
import pytz
from time import sleep
from db import getGreyhoundRace
import requests, socket, traceback
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}


def Grey1SheetController(trade_data, dataQueue):
    try:
        wb1 = xw.Book("greyhounds1_becky.xlsx")
        settings_sheet_becky = wb1.sheets['settings']
        trading_sheet_becky = wb1.sheets['trading']
        results_sheet_becky = wb1.sheets['results']
        wb2 = xw.Book("greyhounds1.xlsx")
        settings_sheet = wb2.sheets['settings']
        results_sheet = wb2.sheets['results']
        trading_sheet = wb2.sheets['trading']
        # add sheets
        print("started")
        # print(trade_data)
        start_data = trade_data.get('grey2Trades')

        sheets = [{'trading': trading_sheet_becky, 'results': results_sheet_becky, 'settings': settings_sheet_becky, 'current_market_id': "", "current_settings_row": 0, "current_prices_logged": False}, {
            'trading': trading_sheet, 'results': results_sheet, 'settings': settings_sheet, 'current_market_id': "", "current_settings_row": 0, "current_prices_logged": False}]

        for sheet in sheets:
            last_times = {}
            for index, event in enumerate(start_data):
                #print(event)
                track_name = event['track']['name'] if f"{event['track']['name']}2" not in last_times else f"{event['track']['name']}2"
                sheet['settings'][f'B{index+2}'].value = f"{track_name}-{event['starttime']}"
                sheet['settings'][f'C{index+2}'].value = f"{event['_id']}"
                if 'trade' in event:
                    sheet['settings'][f'E{index+2}'].value = f"{event['trade']['positions'][0]['runnerName']}"
                if event['results'] != "":
                    try:
                        sheet['settings'][f'D{index+2}'].value = int(str(event['results']).replace(".", ""))
                    except AttributeError:
                        sheet['settings'][f'D{index+2}'].value = race['results']
                if f"{event['track']['name']}2" not in last_times and event['track']['name'] not in last_times:
                    last_times[event['track']['name']] = event['starttime']
                elif event['starttime'] - last_times[event['track']['name']] < 3600:
                    last_times[event['track']['name']] = event['starttime']
                else:
                    last_times[f"{event['track']['name']}2"] = event['starttime']
        my_session = mysqlx.get_session({
            'host': DB_HOST, 'port': DB_PORT,
            'user': DB_USER, 'password': DB_PASS
        })

        my_schema = my_session.get_schema('in4freedom')

        collection = my_schema.get_collection("greyhound_races")
        current_event = ""
        bet_refs = []
        running = True
        while running:
            event_array = trading_sheet_becky['A1'].value.split(" - ")
            for sheet in sheets:
                stakes = trade_data.get("stakes")
                if str(int(sheet['settings']['Q1'].value)) in stakes:
                    stakes = stakes[str(int(sheet['settings']['Q1'].value))]
                #print(stakes)
                #sheet['trading']['S1'].value = stakes['Greyhounds 1']
                sheet['trading']['S1'].value = 0.2
                trading_sheet = sheet['trading']
                settings_sheet = sheet['settings']
                if sheet['current_market_id'] != trading_sheet_becky['N3'].value:
                    sheet['current_prices_logged'] = False
                    sheet['current_market_id'] = trading_sheet_becky['N3'].value
                    

                    date_array = event_array[0].split(" ")
                    time_array = event_array[1].split(" ")
                    today = date.today()
                    year = today.year
                    start_time = int(datetime.timestamp(datetime.strptime(
                        f"{date_array[-2][:-2]}-{date_array[-1]}-{year} {time_array[0]}", "%d-%b-%Y %H:%M")))
                    track_name = " ".join(date_array[:len(date_array)-2])
                    event = next(
                        (item for item in trade_data['greyhoundEvents'] if item["track"]['name'].lower() == track_name.lower() and item['starttime'] == start_time), None)
                    if event is not None:
                        for x in range(2, 1000):  # loop through a range
                            if settings_sheet[f"C{x}"].value == event['_id']:

                                settings_sheet[f"A{x}"].value = sheet['current_market_id']
                                sheet['current_settings_row'] = x
                                runners = []
                                for i in range(7):
                                    runner_name = trading_sheet_becky[f"A{i+5}"].value
                                    if runner_name is None:
                                        break
                                    #print(runner_name)
                                    runners.append(
                                        {'name': runner_name, 'selectionId': trading_sheet_becky[f"Y{i+5}"].value, "trap": int(runner_name.split(" ", 1)[0].replace(".", ""))})
                                break
                    if sheet['results']['A2'].value is not None and sheet['results']['A2'].value not in bet_refs and (sheet['results']['F2'].value == "RESULT_WON" or sheet['results']['F2'].value == "RESULT_LOST"):
                        for i in range(2, 20):
                            if sheet['results'][f'A{i}'].value not in bet_refs:
                                if current_event == "":
                                    continue
                                if sheet['results'][f'B{i}'].value is None:
                                    continue

                                bet_refs.append(sheet['results'][f'A{i}'].value)
                                if sheet['results'][f'C{i}'].value == "B":
                                    if sheet['results'][f'F{i}'].value == "RESULT_WON":
                                        stake = float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1)
                                    else:
                                        stake = float(sheet['results'][f'E{i}'].value)
                                    liability = stake

                                else:
                                    if sheet['results'][f'F{i}'].value == "RESULT_WON":
                                        stake = float(sheet['results'][f'E{i}'].value)
                                        liability = float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1)
                                        
                                    else:
                                        stake = -1*(float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1))
                                        liability = -1*(float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1))
                                    
                                trade = {
                                    'eventID': current_event['_id'],
                                    'sport': "Greyhounds",
                                    'tradeDateTime': current_event['starttime'],
                                    'positions': [{
                                        'runnerName': sheet['results'][f'B{i}'].value.split(" ", 1)[1],
                                        'strategy': "Greyhounds 2",
                                        'positionType': "back" if sheet['results'][f'C{i}'].value == "B" else "lay",
                                        'odds': float(sheet['results'][f'E{i}'].value),
                                        'stake': stake,
                                        "account": "Betfair",
                                        "liability": liability,
                                        "profit": 0.98*float(sheet['results'][f'D{i}'].value),
                                        "result": "win" if  sheet['results'][f'F{i}'].value == "RESULT_WON" else "loss"
                                    }],
                                    "userID": sheet['settings']['Q1'].value,
                                    "sheet": sheet['settings']['Q2'].value,
                                    "betID": sheet['results'][f'A{i}'].value
                                }
                                #print("added trade")
                                dataQueue.put(json.dumps(
                                    {'task': "add_trade_to_log", "data": trade}))
                            else:
                                break
                    current_event = ""

                secs_till_off = trading_sheet['D2'].value
                while True:
                    try:
                        if secs_till_off <= 0.001388 and sheet['current_prices_logged'] is False:
                            #print(trading_sheet['AA12'].value)
                            settings_sheet[f"F{sheet['current_settings_row']}"].value = trading_sheet['AA13'].value
                            settings_sheet[f"G{sheet['current_settings_row']}"].value = trading_sheet['AA14'].value
                            settings_sheet[f"H{sheet['current_settings_row']}"].value = trading_sheet['AA15'].value
                            sheet['current_prices_logged'] = True
                        break
                    except TypeError:
                        continue

                if current_event == "":
                    event_array = trading_sheet_becky['A1'].value.split(" - ")

                    date_array = event_array[0].split(" ")
                    time_array = event_array[1].split(" ")
                    today = date.today()
                    year = today.year
                    start_time = int(datetime.timestamp(datetime.strptime(
                        f"{date_array[-2][:-2]}-{date_array[-1]}-{year} {time_array[0]}", "%d-%b-%Y %H:%M")))
                    track_name = " ".join(date_array[:len(date_array)-2])
                    current_event = next(
                        (item for item in trade_data['greyhoundEvents'] if item["track"]['name'].lower() == track_name.lower() and item['starttime'] == start_time), None)

                for i in range(2, 1000):
                    if settings_sheet[f'B{i}'].value is None:
                        break
                    
                    race_timestamp = int(
                        settings_sheet[f'B{i}'].value.split("-")[1])
                    if int(datetime.now().timestamp()) < race_timestamp + (60*5):
                        continue
                    if settings_sheet[f'D{i}'].value is not None:
                        continue

                    race = collection.find(
                        f"_id = '{settings_sheet[f'C{i}'].value}'").execute()

                    race = race.fetch_one()
                    #print(race)
                    if race is None:
                        continue
                    if race['results'] != "":
                        try:
                            settings_sheet[f'D{i}'].value = int(
                                str(race['results']).replace(".", ""))
                        except AttributeError:
                            settings_sheet[f'D{i}'].value = race['results']



                
            sleep(5)
            print('running greys 1')

        my_session.close()
    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Trading Greyhound Strategy 2",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
