from __future__ import print_function
import mysqlx
from datetime import datetime, date, timedelta
import time
from multiprocessing import Process, JoinableQueue, Queue, Manager, Lock
import traceback
import json
import os
import pytz
from time import sleep
from googleapiclient.discovery import build
import googleapiclient
from google.oauth2 import service_account
import requests, socket, traceback
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")

API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}
def addSportsTrade(trade, my_schema):

    collection = my_schema.get_collection("sports_trades")

    if "_id" in trade:
        collection.modify(f"_id = '{trade['_id']}'").patch(trade).execute()
    else:
        collection.add(trade).execute()
def get_next_trade_row(spreadsheet_service, sheet):
    range_name = f"'Trades'!A11:A"

    data = spreadsheet_service.spreadsheets().values().get(
        spreadsheetId=sheet, range=range_name).execute()
    data_rows = data.get('values', [])

    return len(data_rows) + 11


def write_to_google_range(range_name, values, spreadsheet_service, sheet):

    value_input_option = 'USER_ENTERED'
    body = {
        'values': values
    }

    result = spreadsheet_service.spreadsheets().values().update(
        spreadsheetId=sheet, range=range_name,
        valueInputOption=value_input_option, body=body).execute()
    print(result)
    # print('{0} cells updated.'.format(result.get('updatedCells')))


def sheets_from_db(users, my_schema):
    user_string = ""
    for user in users:
        user_string = f"{user_string} {user},"
    user_table = my_schema.get_table('users')

    user_settings_collection = my_schema.get_collection('user_settings')

    users = user_table.select().where(
        f"userID IN ({user_string[:-1]})").execute()

    users = users.fetch_all()

    users_details = []

    for user in users:
        settings = user_settings_collection.find(
            f"userID = {user[0]}").execute()

        settings = settings.fetch_one()
        # print(settings)
        if settings is None:
            settings = {'userID': user[0]}
            user_settings_collection.add(settings).execute()
            settings = user_settings_collection.find(
                f"userID = {user[0]}").execute()

            settings = settings.fetch_one()
            # print(settings)
        if "bankrolls" not in settings:
            settings['bankrolls'] = [{
                'strategy': "Betting",
                'balance': 0,
                'unitSize': 0
            }, {
                'strategy': "Greyhounds 1",
                'balance': 0,
                'unitSize': 0
            }, {
                'strategy': "Greyhounds 2",
                'balance': 0,
                'unitSize': 0
            }, {
                'strategy': "Over 2.5",
                'balance': 0,
                'unitSize': 0
            }, {
                'strategy': "Dutching Correct Score",
                'balance': 0,
                'unitSize': 0
            },
                {
                'strategy': "Horses",
                'balance': 0,
                'unitSize': 0
            }]
            # print(settings)
            user_settings_collection.modify(
                f"userID = {user[0]}").patch(settings).execute()

            settings = user_settings_collection.find(
                f"userID = {user[0]}").execute()

            settings = settings.fetch_one()
            # print(settings)
        users_details.append(
            {'sheet': user[6], 'bankrolls': settings['bankrolls'], 'userID': user[0]})

    return users_details


def get_bankrolls_from_sheet(sheet, spreadsheet_service):
    range_name = f"'Accounts'!L2:N"

    data = spreadsheet_service.spreadsheets().values().get(
        spreadsheetId=sheet, range=range_name).execute()
    data_rows = data.get('values', [])

    header_range = "'Accounts'!L1:N1"
    header_data = spreadsheet_service.spreadsheets().values().get(
        spreadsheetId=sheet, range=header_range).execute()

    header_data = header_data.get('values', [])

    return header_data[0], data_rows


def calc_new_units_sizes(old_bankroll, new_bankroll, factors):
    print(old_bankroll)
    print(float(new_bankroll[1].replace("£", "").replace(",", ""))/factors[new_bankroll[0]])
    #return float(new_bankroll[1].replace("£", "").replace(",", ""))/factors[new_bankroll[0]]

    if float(new_bankroll[1].replace("£", "").replace(",", ""))/factors[new_bankroll[0]] > float(old_bankroll['unitSize']):
        return float(new_bankroll[1].replace("£", "").replace(",", ""))/factors[new_bankroll[0]]
    else:
        return old_bankroll['unitSize']


def change_units_on_sheet(new_bankroll_rows, sheet, spreadsheet_service):
    range_name = f"'Accounts'!N2:N"
    value_input_option = 'USER_ENTERED'
    body = {
        'values': new_bankroll_rows
    }

    result = spreadsheet_service.spreadsheets().values().update(
        spreadsheetId=sheet, range=range_name,
        valueInputOption=value_input_option, body=body).execute()
    # print('{0} cells updated.'.format(result.get('updatedCells')))


def change_units_in_db(bankroll_rows, my_schema, userID):
    user_settings_collection = my_schema.get_collection('user_settings')
    new_bankrolls = []

    for bankroll in bankroll_rows:
        new_bankrolls.append(
            {"strategy": bankroll[0], "bankroll": bankroll[1], 'unitSize': bankroll[2]})
    user_settings_collection.modify(f"userID = {userID}").set(
        "bankrolls", new_bankrolls).execute()


def BankRollController(trade_data, dataQueue):
    try:
        DIRECTORY = os.path.dirname(os.path.abspath(__file__))

        SCOPES = [
            'https://www.googleapis.com/auth/spreadsheets',
            'https://www.googleapis.com/auth/drive'
        ]

        credentials = service_account.Credentials.from_service_account_file(
            f'{DIRECTORY}/client_secret.json', scopes=SCOPES)
        spreadsheet_service = build('sheets', 'v4', credentials=credentials)

        FACTORS = {
            'Betting': 100, 'Greyhounds 1': 4000, 'Greyhounds 2': 200, "Over 2.5": 100, "Dutching Correct Score": 50, "Horses": 500
        }
        my_session = mysqlx.get_session({
            'host': DB_HOST, 'port': DB_PORT,
            'user': DB_USER, 'password': DB_PASS
        })

        my_schema = my_session.get_schema('in4freedom')
        running = True
        while running:
            
            trades = trade_data["tradesToLog"]
            #print(trades)
            trades_to_delete = []
            
            todays_date = datetime.now().strftime("%Y-%m-%d")
            
            for key, trade in trades.items():
                next_row = get_next_trade_row(spreadsheet_service, trade['sheet'])
                trade_rows=[]
                trades_to_delete.append(key)
                
                for position in trade['positions']:
                    if position['result'] == "win":
                        result = "Y"
                    else:
                        result = "N"

                    if position['positionType'] == "Lay":
                        lay = "Y"
                        if result == "Y":

                            payout = position['stake']+position['liability']
                        else:

                            payout = 0

                    else:
                        lay = "N"
                        if result == "Y":
                            payout = position['stake']*(position['odds'])

                        else:
                            payout = 0
                    print([
                        todays_date,
                        position['account'],
                        position['strategy'],
                        position['stake'],
                        position['odds'],
                        "",
                        result,
                        0.02,
                        lay,
                        "",
                        position['liability'],
                        position['stake']*position['odds'],
                        payout,
                        position['profit'],
                        f"{key}"])

                    trade_rows.append([
                        todays_date,
                        position['account'],
                        position['strategy'],
                        position['stake'],
                        position['odds'],
                        "",
                        result,
                        0.02,
                        lay,
                        "",
                        position['liability'],
                        position['stake']*position['odds'],
                        payout,
                        position['profit'],
                        f"{key}"])
                print(trade_rows)
                write_to_google_range(f"Trades!A{next_row}:P{next_row+len(trade_rows)}",
                                    trade_rows, spreadsheet_service, trade['sheet'])

                addSportsTrade(trade, my_schema)
            dataQueue.put(json.dumps(
                {"task": "delete_trades_from_log", "data": trades_to_delete}))
            users = [2, 3, 4]
            users_details = sheets_from_db(users, my_schema)
            new_stakes = {}
            for user in users_details:
                new_stakes[user['userID']] = {}
                headers, bankrolls = get_bankrolls_from_sheet(
                    user['sheet'], spreadsheet_service)
                print(bankrolls)
                new_bankroll_rows = []
                new_unit_rows = []
                for bankroll in bankrolls:
                    old_bankroll = next(
                        (item for item in user['bankrolls'] if item['strategy'] == bankroll[0]), None)

                    new_unit_size = calc_new_units_sizes(
                        old_bankroll, bankroll, FACTORS)
                    new_unit_rows.append([new_unit_size])
                    new_bankroll_rows.append([bankroll[0], bankroll[1], new_unit_size])
                    new_stakes[user['userID']][bankroll[0]
                                            ] = new_unit_size
                print(new_bankroll_rows)
                change_units_on_sheet(
                    new_unit_rows, user['sheet'], spreadsheet_service)

                change_units_in_db(new_bankroll_rows, my_schema, user['userID'])
            dataQueue.put(json.dumps({"task": "updateStakes", "data": new_stakes}))
            if running is False:
                break
            sleep(180)

        my_session.close()
    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Updating Sports Trading Bots Bankroll",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)


        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})

