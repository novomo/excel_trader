import xlwings as xw
import mysqlx
import pywintypes
from datetime import datetime, date, timedelta, timezone
import time
import traceback
import json
import os
import pytz
from time import sleep
from db import getGreyhoundRace
import requests
import socket
import traceback
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}


def updateGreyhoundRace(marketId, eventId, runners, my_schema):
    try:
        collection = my_schema.get_collection("greyhound_races")

        race = collection.find(f"_id = '{eventId}'").execute()

        race = race.fetch_one()
        newRunners = []
        # print(runners)
        for runner in runners:
            # print(runner)
            if runner is None:
                continue
            try:
                new_runner = runner
                new_runner['selectionID'] = int(runner['selectionId'])
            except ValueError:
                new_runner = runner
            newRunners.append(new_runner)
        # print(newRunners)
        # print(marketId)
        try:
            collection.modify(f"_id = '{eventId}'").patch({
                "marketID": marketId, 'runners': newRunners}).execute()
            race = collection.find(f"_id = '{eventId}'").execute()
        except mysqlx.errors.InterfaceError:
            my_session.close()
            sleep(2)
            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            my_schema = my_session.get_schema('in4freedom')
            collection = my_schema.get_collection("greyhound_races")
            collection.modify(f"_id = '{eventId}'").patch({
                "marketID": marketId, 'runners': newRunners}).execute()
            race = collection.find(f"_id = '{eventId}'").execute()
        race = race.fetch_one()

        # print(race)
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Updating Greyhound entry",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})


def updateGreyhoundRaceFinalData(eventId, new_data, my_schema):
    print(new_data)
    try:
        collection = my_schema.get_collection("greyhound_races")

        race = collection.find(f"_id = '{eventId}'").execute()

        race = race.fetch_one()
        newRunners = []
        print(race)
        print(new_data['runner_data'])
        try:
            for runner in new_data['runner_data']:
                # print(runner)
                if runner is None:
                    continue
                r = next(
                    (item for item in race['runners'] if item['name'].lower() == runner["name"].lower()), None)
                print(r)
                if r is None:
                    continue
                else:
                    try:
                        new_runner = r
                        new_runner['startBackPriced'] = runner['startBackPriced']
                        new_runner['endedBackPriced'] = runner['endedBackPriced']
                        new_runner['startTraded'] = runner['startTraded']
                        new_runner['endedTraded'] = runner['endedTraded']
                        new_runner['lineMove'] = (
                            1/runner['startBackPriced']) * runner['endedBackPriced']
                        new_runner['startPerTraded'] = (
                            1/new_data['startTraded'])*runner['startTraded']
                        new_runner['endedPerTraded'] = (
                            1/new_data['endedTraded'])*runner['endedTraded']
                    except ValueError:
                        continue
                newRunners.append(new_runner)
            print(newRunners)
            # print(marketId)
            if len(newRunners) > 0:
                new_data['runners'] = newRunners
            del new_data['runners_data']
        except KeyError:
            pass
        collection.modify(f"_id = '{eventId}'").patch(new_data).execute()
        race = collection.find(f"_id = '{eventId}'").execute()

        race = race.fetch_one()

        print(race)
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Updating Greyhound entry",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})


def Grey2SheetController(trade_data, dataQueue):
    try:
        wb1 = xw.Book("greyhounds2_becky.xlsx")
        settings_sheet_becky = wb1.sheets['settings']
        trading_sheet_becky = wb1.sheets['trading']
        results_sheet_becky = wb1.sheets['results']
        wb2 = xw.Book("greyhounds2.xlsx")
        settings_sheet = wb2.sheets['settings']
        results_sheet = wb2.sheets['results']
        trading_sheet = wb2.sheets['trading']
        # add sheets
        print("started")
        # print(trade_data)
        start_data = trade_data.get('grey2Trades')

        sheets = [{'trading': trading_sheet_becky, 'results': results_sheet_becky, 'settings': settings_sheet_becky, 'current_market_id': "", "current_settings_row": 0, "current_prices_logged": False, 'new_data': {}, 'new_data_logged': False}, {
            'trading': trading_sheet, 'results': results_sheet, 'settings': settings_sheet, 'current_market_id': "", "current_settings_row": 0, "current_prices_logged": False, 'new_data': {}, 'new_data_logged': False}]

        for sheet in sheets:
            last_times = {}
            for index, event in enumerate(start_data):
                fastest_time = 1000
                fastest_trap = 0
                for runner in event['runners']:
                    if "distance_trap_grade_actual" in runner and runner['distance_trap_grade_actual'] < fastest_time and runner['distance_trap_grade_actual'] != 0:
                        fastest_trap = runner['greyhound']['name']
                        fastest_time = runner['distance_trap_grade_actual']

                # print(event)
                if f"{event['course']['name']}2" not in last_times and event['course']['name'] not in last_times:
                    last_times[event['course']['name']] = event['starttime']
                elif event['starttime'] - last_times[event['course']['name']] < 3600:
                    last_times[event['course']['name']] = event['starttime']
                else:
                    last_times[f"{event['course']['name']}2"] = event['starttime']
                track_name = event['course']['name'] if f"{event['course']['name']}2" not in last_times else f"{event['course']['name']}2"
                sheet['settings'][f'B{index+2}'].value = f"{track_name}-{event['starttime']}"
                sheet['settings'][f'C{index+2}'].value = f"{event['_id']}"
                sheet['settings'][f'A{index+2}'].value = f"{event['marketID']}"
                sheet['settings'][f'I{index+2}'].value = track_name.replace(
                    "-", " ").capitalize()
                sheet['settings'][f'J{index+2}'].value = event['predictions'][0]
                sheet['settings'][f'K{index+2}'].value = event['predictions'][1]
                try:
                    sheet['settings'][f'L{index+2}'].value = event['predictions'][2]
                except IndexError:
                    pass
                sheet['settings'][f'M{index+2}'].value = fastest_trap
                if 'trade' in event:
                    sheet['settings'][f'E{index+2}'].value = f"{event['trade']['positions'][0]['runnerName']}"
                if "results" in event and event['results'] != "":
                    try:
                        sheet['settings'][f'D{index+2}'].value = int(
                            str(event['results']).replace(".", ""))
                    except AttributeError:
                        sheet['settings'][f'D{index+2}'].value = race['results']

        my_session = mysqlx.get_session({
            'host': DB_HOST, 'port': DB_PORT,
            'user': DB_USER, 'password': DB_PASS
        })

        my_schema = my_session.get_schema('in4freedom')

        collection = my_schema.get_collection("greyhound_races")
        current_event = ""
        bet_refs = []
        running = True

        while running:
            event_array = trading_sheet_becky['A1'].value.split(" - ")
            for sheet in sheets:
                stakes = trade_data.get("stakes")
                if str(int(sheet['settings']['Q1'].value)) in stakes:
                    stakes = stakes[str(int(sheet['settings']['Q1'].value))]
                # print(stakes)
                # sheet['trading']['S1'].value = stakes['Greyhounds 2']
                sheet['trading']['S1'].value = stakes['Greyhounds 2'] = 0.2
                trading_sheet = sheet['trading']
                settings_sheet = sheet['settings']
                if sheet['current_market_id'] != trading_sheet_becky['N3'].value:
                    sheet['current_prices_logged'] = False
                    sheet['current_market_id'] = trading_sheet_becky['N3'].value
                    sheet['new_data'] = {}
                    sheet['new_data_logged'] = False

                    date_array = event_array[0].split(" ")
                    time_array = event_array[1].split(" ")
                    today = date.today()
                    year = today.year
                    start_time = int(datetime.timestamp(datetime.strptime(
                        f"{date_array[-2][:-2]}-{date_array[-1]}-{year} {time_array[0]}", "%d-%b-%Y %H:%M")))
                    print(start_time)
                    track_name = " ".join(date_array[:len(date_array)-2])
                    event = next(
                        (item for item in trade_data['greyhoundEvents'] if str(item["marketID"]) == str(int(trading_sheet_becky['N3'].value))), None)
                    print(track_name)
                    print(str(int(trading_sheet_becky['N3'].value)))
                    print(event)
                    if event is not None:
                        for x in range(2, 1000):  # loop through a range
                            # print(settings_sheet[f"C{x}"].value)
                            if settings_sheet[f"C{x}"].value == event['_id']:

                                settings_sheet[f"A{x}"].value = sheet['current_market_id']
                                trading_sheet['AA15'].value = settings_sheet[f"H{x}"].value
                                sheet['current_settings_row'] = x
                                runners = []
                                for i in range(7):
                                    runner_name = trading_sheet_becky[f"A{i+5}"].value
                                    if runner_name is None:
                                        break
                                    # print(runner_name)
                                    runners.append(
                                        {'name': runner_name.split(" ", 1)[1], 'selectionId': trading_sheet_becky[f"Y{i+5}"].value, "trap": int(runner_name.split(" ", 1)[0].replace(".", ""))})
                                # updateGreyhoundRace(
                                    # sheet['current_market_id'], event['_id'], runners, my_schema)
                                break
                    if sheet['results']['A2'].value is not None and sheet['results']['A2'].value not in bet_refs:
                        for i in range(2, 20):
                            if sheet['results'][f'A{i}'].value not in bet_refs:
                                if current_event == "" or current_event is None:
                                    continue
                                if sheet['results'][f'B{i}'].value is None:
                                    continue
                                if sheet['results']['F2'].value != "RESULT_WON" and sheet['results']['F2'].value != "RESULT_LOST":
                                    continue

                                bet_refs.append(
                                    sheet['results'][f'A{i}'].value)
                                if sheet['results'][f'C{i}'].value == "B":
                                    if sheet['results'][f'F{i}'].value == "RESULT_WON":
                                        stake = float(
                                            sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1)
                                    else:
                                        stake = float(
                                            sheet['results'][f'E{i}'].value)
                                    liability = stake

                                else:
                                    if sheet['results'][f'F{i}'].value == "RESULT_WON":
                                        stake = float(
                                            sheet['results'][f'E{i}'].value)
                                        liability = float(
                                            sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1)

                                    else:
                                        stake = -1 * \
                                            (float(
                                                sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1))
                                        liability = -1 * \
                                            (float(
                                                sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1))
                                print(sheet['results'][f'F{i}'].value)
                                trade = {
                                    'eventID': current_event['_id'],
                                    'sport': "Greyhounds",
                                    'tradeDateTime': current_event['starttime'],
                                    'positions': [{
                                        'runnerName': sheet['results'][f'B{i}'].value.split(" ", 1)[1],
                                        'strategy': "Greyhounds 2",
                                        'positionType': "back" if sheet['results'][f'C{i}'].value == "B" else "lay",
                                        'odds': float(sheet['results'][f'E{i}'].value),
                                        'stake': stake,
                                        "account": "Betfair",
                                        "liability": liability,
                                        "profit": 0.98*float(sheet['results'][f'D{i}'].value) if float(sheet['results'][f'D{i}'].value) > 0 else float(sheet['results'][f'D{i}'].value),
                                        "result": "win" if sheet['results'][f'F{i}'].value == "RESULT_WON" else "loss"
                                    }],
                                    "userID": sheet['settings']['Q1'].value,
                                    "sheet": sheet['settings']['Q2'].value,
                                    "betID": sheet['results'][f'A{i}'].value
                                }
                                print("added trade")
                                dataQueue.put(json.dumps(
                                    {'task': "add_trade_to_log", "data": trade}))
                            else:
                                break
                    current_event = ""

                # while True:
                    # try:
                try:
                    secs_till_off = float(trading_sheet['D2'].value)*86400
                except ValueError:
                    secs_till_off = 0
                print(trading_sheet['D2'].value)
                print(secs_till_off)
                if secs_till_off <= 120 and sheet['current_prices_logged'] is False:
                    print(sheet['current_settings_row'])
                    if sheet['current_settings_row'] == 0:
                        continue
                    settings_sheet[f"F{sheet['current_settings_row']}"].value = trading_sheet['AA13'].value

                    settings_sheet[f"G{sheet['current_settings_row']}"].value = trading_sheet['AA14'].value
                    settings_sheet[f"H{sheet['current_settings_row']}"].value = trading_sheet['AA15'].value
                    sheet['current_prices_logged'] = True
                    t_name = trading_sheet['AA15'].value
                    biasLowerPer = 0
                    biasUpperPer = 0
                    for i in range(2, 20):
                        if settings_sheet[f'AA{i}'].value is None:
                            break
                        if settings_sheet[f'AA{i}'].value == t_name:
                            try:
                                biasLowerPer = float(
                                    settings_sheet[f'AB{i}'].value)
                                biasUpperPer = float(
                                    settings_sheet[f'AC{i}'].value)
                            except TypeError:
                                biasLowerPer = 'none'
                                biasUpperPer = 'none'
                            break

                    sheet['new_data']['bias'] = trading_sheet['AA16'].value
                    sheet['new_data']['biasLowerPer'] = biasLowerPer
                    sheet['new_data']['biasUpperPer'] = biasUpperPer
                    sheet['new_data']['startTraded'] = float(
                        trading_sheet['AA2'].value)
                    sheet['new_data']['runner_data'] = []
                    print(sheet['new_data'])
                    for x in range(5, 13):
                        print(f'A{x}')
                        if sheet['trading'][f'A{x}'].value is None:
                            break
                        print(event)
                        print(sheet['trading'][f'A{x}'].value)

                        r = next((item for item in event['runners'] if item["greyhound"]['name'].lower(
                        ) in sheet['trading'][f'A{x}'].value.lower()), None)
                        sheet['new_data']['runner_data'].append({
                            'name': sheet['trading'][f'A{x}'].value.split(". ", 1)[1],
                            'startBackPriced': float(sheet['trading'][f'F{x}'].value),
                            'startTraded': float(sheet['trading'][f'P{x}'].value)
                        })
                        sheet['trading'][f'D{x+16}'].value = float(
                            sheet['trading'][f'F{x}'].value)
                        if r is not None and 'mstrSpd' in r:
                            sheet['trading'][f'Q{x+16}'].value = r['mstrSpd']

                        # break
                    # except TypeError:
                        # continue
                # while True:
                    # try:
                try:
                    secs_till_off = float(trading_sheet['D2'].value)*86400
                except ValueError:
                    secs_till_off = 0
                print(trading_sheet['D2'].value)
                print(secs_till_off)
                if secs_till_off <= 10 and sheet['new_data_logged'] is False:
                    sheet['new_data_logged'] = True
                    sheet['new_data']['endedTraded'] = float(
                        trading_sheet['AA2'].value)
                    try:
                        for i, runner in enumerate(sheet['new_data']['runner_data']):
                            if sheet['trading'][f'A{i+5}'].value is None:
                                break
                            sheet['new_data']['runner_data'][i]['endedBackPriced'] = float(
                                sheet['trading'][f'F{i+5}'].value)
                            sheet['new_data']['runner_data'][i]['endedTraded'] = float(
                                sheet['trading'][f'P{i+5}'].value)
                    except KeyError:
                        pass
                    try:
                        updateGreyhoundRaceFinalData(
                            event['_id'], sheet['new_data'], my_schema)
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection(
                            "greyhound_races")
                        updateGreyhoundRaceFinalData(
                            event['_id'], sheet['new_data'], my_schema)

                        # break
                    # except TypeError:
                        # continue

                if current_event == "":
                    event_array = trading_sheet_becky['A1'].value.split(" - ")

                    date_array = event_array[0].split(" ")
                    time_array = event_array[1].split(" ")
                    today = date.today()
                    year = today.year
                    start_time = int(datetime.timestamp(datetime.strptime(
                        f"{date_array[-2][:-2]}-{date_array[-1]}-{year} {time_array[0]}", "%d-%b-%Y %H:%M")))
                    track_name = " ".join(date_array[:len(date_array)-2])
                    current_event = next(
                        (item for item in trade_data['greyhoundEvents'] if item["course"]['name'].lower() == track_name.lower().replace(" ", "-") and item['starttime'] == start_time), None)

                for i in range(2, 1000):
                    if settings_sheet[f'B{i}'].value is None:
                        break
                    # print(settings_sheet[f'B{i}'].value)
                    race_timestamp = int(
                        settings_sheet[f'B{i}'].value.split("-")[-1])
                    if int(datetime.now().timestamp()) < race_timestamp + (60*5):
                        continue
                    if settings_sheet[f'D{i}'].value is not None:
                        continue
                    try:
                        race = collection.find(
                            f"_id = '{settings_sheet[f'C{i}'].value}'").execute()
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection(
                            "greyhound_races")
                        race = collection.find(
                            f"_id = '{settings_sheet[f'C{i}'].value}'").execute()

                    race = race.fetch_one()
                    # print(race)
                    if race is None:
                        continue
                    if "results" in race and race['results'] != "":
                        try:
                            settings_sheet[f'D{i}'].value = int(
                                str(race['results']).replace(".", ""))
                        except AttributeError:
                            settings_sheet[f'D{i}'].value = race['results']

            sleep(5)
            print('running greys 2')

        my_session.close()
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Trading Greyhound Strategy 2",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
