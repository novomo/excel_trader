import mysqlx
from dotenv import load_dotenv
import os
from datetime import date
from datetime import datetime
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
print(DB_HOST)
print(DB_PORT)
print(DB_USER)
print(DB_PASS)

my_session = mysqlx.get_session({
    'host': DB_HOST, 'port': DB_PORT,
    'user': DB_USER, 'password': DB_PASS
})

my_schema = my_session.get_schema('in4freedom')

race_collection = my_schema.get_collection("horse_races")
now = datetime.now()
new_now = now.replace(hour=0, minute=0, second=0)
todays_date_string = (
    date.today()).strftime("%Y-%m-%d")
races = race_collection.find(
    f"starttime > {int(datetime.now().timestamp())}").sort('starttime asc').execute()

races = races.fetch_all()
print(races)
