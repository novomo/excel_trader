from __future__ import print_function
import xlwings as xw
import asyncio
import logging
import mysqlx
from datetime import datetime, date, timedelta, timezone
import time
from gql import Client, gql
from gql.transport.websockets import WebsocketsTransport
from gql.transport.aiohttp import AIOHTTPTransport
from multiprocessing import Process, JoinableQueue, Queue, Manager, Lock
import traceback
import json
import os
import pytz
from time import sleep
from googleapiclient.discovery import build
import googleapiclient
from google.oauth2 import service_account
from greys1 import Grey1SheetController
from greys2 import Grey2SheetController
from over25 import Over25SheetController
from horses import HorseSheetController
from db import getGreyhoundRace
from excel_trader.old.bankroll import BankRollController
import requests
import socket
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")


API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}

SHEET_NAME = "trader.xlsx"


def addPosition(data, trade_data):
    trade_data['trades'][data['eventID']]['positions'].append(data['position'])


def addTrade(data, trade_data):
    trade_data['trades'][data['eventID']] = data['trade']


def updateTrade(data, trade_data):
    trade_data['trades'][data['eventID']][data['key']] = data['value']


def addGrey2(data, trade_data):
    trade_data['grey2Trades'] = data


def addOver25(data, trade_data):
    trade_data['over25Trades'] = data


def addDutching(data, trade_data):
    trade_data['dutchingTrades'] = data


def deleteGrey2(data, trade_data):
    trade_data['grey2Trades'] = data


def addGreyhoundEvents(data, trade_data):
    trade_data["greyhoundEvents"] = data


def addHorseEvents(data, trade_data):
    trade_data["horseEvents"] = data


def addTradeToLog(data, trade_data):
    newTrades = trade_data.get('tradesToLog')
    print(newTrades)
    newTrades[str(int(data['betID']))] = data
    trade_data['tradesToLog'] = newTrades


def deleteTrades(data, trade_data):
    newTrades = trade_data.get('tradesToLog')
    print(newTrades)
    print(data)
    for tradeID in data:
        print(tradeID)
        if tradeID in newTrades:
            del newTrades[tradeID]
    print(newTrades)
    trade_data['tradesToLog'] = newTrades
    print(trade_data['tradesToLog'])


def updateStakes(data, trade_data):
    trade_data['stakes'] = data


def dataTaskConsumer(queue, doneQueue, trade_data):
    while True:
        nextTask = queue.get()
        if nextTask == 'finished':
            doneQueue.put('finished')
            break
        try:
            task_info = json.loads(nextTask)

            if task_info['task'] == "add_position":
                addPosition(task_info['data'], trade_data)
            elif task_info['task'] == "add_trade":
                addTrade(task_info['data'], trade_data)
            elif task_info['task'] == "update_trade":
                updateTrade(task_info['data'], trade_data)
            elif task_info['task'] == "start_grey2":
                addGrey2(task_info['data'], trade_data)
            elif task_info['task'] == "start_over25":
                addOver25(task_info['data'], trade_data)
            elif task_info['task'] == "start_dutching":
                addDutching(task_info['data'], trade_data)
            elif task_info['task'] == "greyhound_events":
                addGreyhoundEvents(task_info['data'], trade_data)
            elif task_info['task'] == "delete_grey2":
                deleteGrey2(task_info['data'], trade_data)
            elif task_info['task'] == "add_trade_to_log":
                print(task_info)
                addTradeToLog(task_info['data'], trade_data)

            elif task_info['task'] == "delete_trades_from_log":
                print(task_info)
                deleteTrades(task_info['data'], trade_data)
            elif task_info['task'] == "updateStakes":
                updateStakes(task_info['data'], trade_data)
            elif task_info['task'] == "horserace_events":
                addHorseEvents(task_info['data'], trade_data)
        except:
            traceback.print_exc()

            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                            errorTitle: "Parsing Data Queue Information in Excel Trader",
                            machine: "{ip_address}",
                            machineName: "API",
                            errorFileName: "{file}",
                            err: "{err},
                            critical: true
                        }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        queue.task_done()


def updateGreyhoundRace(marketId, eventId, runners, my_schema):
    try:
        collection = my_schema.get_collection("greyhound_races")

        race = collection.find(f"_id = '{eventId}'").execute()

        race = race.fetch_one()
        newRunners = []
        for runner in runners:
            print(runner)
            if runner is None:
                continue
            r = next(
                (item for item in race['runners'] if item['trap'] == int(runner["name"].split(" ")[0].replace(".", ""))), None)
            print(r)
            if r is None:
                new_runner = {"trap": int(runner["name"].split(" ")[0].replace(
                    ".", "")), "name": runner["name"].split(" ", 1)[1].strip(), 'selectionID': runner['selectionId']}
            else:
                new_runner = runner
                new_runner['selectionID'] = int(r['selectionId'])
            newRunners.append(new_runner)
        print(newRunners)
        print(marketId)

        collection.modify(f"_id = '{eventId}'").patch({
            "marketID": marketId, 'runners': newRunners}).execute()
        race = collection.find(f"_id = '{eventId}'").execute()

        race = race.fetch_one()

        print(race)
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Updating Greyhound entry",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})


def DCSSheetController(trade_data, dataQueue):
    try:
        letters = ['E', 'F', 'G', 'H', 'I', 'J']
        wb1 = xw.Book("over25_becky.xlsx")
        trading_sheet_becky = wb1.sheets['trading']
        results_sheet_becky = wb1.sheets['results']
        settings_sheet_becky = wb1.sheets['settings']
        wb2 = xw.Book("over25.xlsx")
        trading_sheet = wb2.sheets['trading']
        results_sheet = wb2.sheets['results']
        settings_sheet = wb2.sheets['settings']

        sheets = [{'trading': trading_sheet_becky, 'results': results_sheet_becky, 'settings': settings_sheet_becky, 'current_market_id': "", "current_settings_row": 0, "new_trades_logged": False}, {
            'trading': trading_sheet, 'results': results_sheet, 'settings': settings_sheet, 'current_market_id': "", "current_settings_row": 0, "new_trades_logged": False}]

        start_data = trade_data.get('over25Trades')

        for sheet in sheets:

            for index, trade in enumerate(start_data):
                sheets['settings'][f'B{index+2}'].value = f"{trade['eventID']}"
                sheets['settings'][f'A{index+2}'].value = f"{trade['_id']}"
                sheet['settings'][f'C{index+2}'].value = f"{trade['event']}"
                sheet['settings'][f'D{index+2}'].value = {trade['tradeDateTime']}
                for i, position in enumerate(trade['positions']):
                    sheets['settings'][f'{letters[i]}{index+2}'].value = f"{position['selection']}"

        bet_refs = []
        running = True
        while running:
            for sheet in sheets:
                stakes = trade_data.get("stakes")
                stakes = stakes[str(int(sheet['settings']['Q1'].value))]
                sheet['trading']['S1'].value = stakes['Dutching Correct Score']
                trading_sheet = sheet['trading']
                settings_sheet = sheet['settings']
                if sheet['current_market_id'] != trading_sheet['N3'].value:
                    if sheet['results']['A2'].value not in bet_refs:
                        for i in range(2, 20):
                            if sheet['results'][f'A{i}'].value not in bet_refs and (sheet['results']['F2'].value == "RESULT_WON" or sheet['results']['F2'].value == "RESULT_LOST"):
                                stakes = trade_data.get("stakes")
                                stakes = stakes[str(
                                    int(sheet['settings']['Q1'].value))]
                                eventID = ""
                                tradeID = ""
                                starttime = 0
                                for x in range(2, 20):
                                    if settings_sheet[f"J{x}"].value == sheet['current_market_id']:
                                        eventID = settings_sheet[f"B{x}"].value
                                        tradeID = settings_sheet[f"A{x}"].value
                                        starttime = settings_sheet[f"D{x}"].value

                                bet_refs.append(
                                    sheet['results'][f'A{i}'].value)
                                trade = {
                                    'eventID': eventID,
                                    '_id': tradeID,
                                    'sport': "Football",
                                    'tradeDateTime': starttime,
                                    'positions': [{
                                        'runnerName': sheet['results'][f'B{i}'].value,
                                        'strategy': "Dutching Correct Score",
                                        'positionType': "back",
                                        'odds': float(sheet['results'][f'E{i}'].value),
                                        'stake': stakes['Dutching Correct Score'],
                                        "account": "Betfair",
                                        "liability": stakes['Dutching Correct Score'],

                                    }],
                                    "userID": sheet['settings']['P1'].value,
                                    "sheet": sheet['settings']['P2'].value,
                                    "betID": sheet['results'][f'A{i}'].value
                                }
                                dataQueue.put(json.dumps(
                                    {'task': "add_trade_to_log", "data": trade}))
                            else:
                                break
                    sheet['current_market_id'] = trading_sheet['N3'].value
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Trading Dutching Correct Score Strategy",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})


async def get_current_trades():

    transport = AIOHTTPTransport(
        url="https://api.in4freedom.com/graphql", headers=HEADERS, timeout=25)

    # Using `async with` on the client will start a connection on the transport
    # and provide a `session` variable to execute queries on this connection
    async with Client(
        transport=transport,
        fetch_schema_from_transport=True,
    ) as session:

        # Execute single query
        trade_query = gql(
            """
            query GetSportsTrade($trade_query: String!) {
            getSportsTrade(query: $trade_query) {
                _id
                eventID
                event
                positions {
                odds
                positionType
                profit
                result
                runnerName
                stake
                strategy
                }
                sport
                tradeDateTime
            }
            
            }
            """
        )
        try:
            final_results = {}
            result = await session.execute(trade_query, variable_values={"trade_query": f"userID = 0 AND tradeDateTime > {int(datetime.now().timestamp())}"})
            # print(result)
            final_results['getSportsTrade'] = result['getSportsTrade']

            final_results['getGreyhoundRaces'] = []
            final_results['getHorseRaces'] = []
            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            my_schema = my_session.get_schema('in4freedom')

            race_collection = my_schema.get_collection("greyhound_races")
            track_collection = my_schema.get_table("greyhound_tracks")
            # now = datetime.now()
            # midnight = now.replace(hour = 4, minute = 0, second = 0, microsecond = 0)
            # races = race_collection.find(
            # f"starttime > {int(midnight.timestamp())}").execute()
            todays_date_string = (
                date.today()).strftime("%Y-%m-%d")

            try:
                races = race_collection.find(
                    f"url like '%{todays_date_string}%'").sort('starttime asc').execute()
            except mysqlx.errors.InterfaceError:
                my_session.close()
                sleep(2)
                my_session = mysqlx.get_session({
                    'host': DB_HOST, 'port': DB_PORT,
                    'user': DB_USER, 'password': DB_PASS
                })

                my_schema = my_session.get_schema('in4freedom')
                race_collection = my_schema.get_collection("greyhound_races")
                track_collection = my_schema.get_table("greyhound_tracks")
                races = race_collection.find(
                    f"url like '%{todays_date_string}%'").sort('starttime asc').execute()

            races = races.fetch_all()

            races = list(races)
            for race in races:
                race = dict(race)
                # track = track_collection.select().where(
                # f"trackID = {race['track']['trackID']}").execute()

                # track = track.fetch_one()
                # race['track']['name'] = track[1]
                # race['track']['starttime'] = track[2]
                final_results['getGreyhoundRaces'].append(race)

            race_collection = my_schema.get_collection("horse_races")
            track_collection = my_schema.get_table("horse_tracks")
            try:
                races = race_collection.find(
                    f"starttime > {int(datetime.now().timestamp())}").execute()
            except mysqlx.errors.InterfaceError:
                my_session.close()
                sleep(2)
                my_session = mysqlx.get_session({
                    'host': DB_HOST, 'port': DB_PORT,
                    'user': DB_USER, 'password': DB_PASS
                })

                my_schema = my_session.get_schema('in4freedom')
                race_collection = my_schema.get_collection("greyhound_races")
                track_collection = my_schema.get_table("greyhound_tracks")
                races = race_collection.find(
                    f"starttime > {int(datetime.now().timestamp())}").execute()

            races = races.fetch_all()

            races = list(races)
            for race in races:
                race = dict(race)
                # print(race)
                try:
                    track = track_collection.select().where(
                        f"courseID = {race['course']['courseID']}").execute()
                except mysqlx.errors.InterfaceError:
                    my_session.close()
                    sleep(2)
                    my_session = mysqlx.get_session({
                        'host': DB_HOST, 'port': DB_PORT,
                        'user': DB_USER, 'password': DB_PASS
                    })

                    my_schema = my_session.get_schema('in4freedom')
                    race_collection = my_schema.get_collection(
                        "greyhound_races")
                    track_collection = my_schema.get_table("greyhound_tracks")
                    track = track_collection.select().where(
                        f"courseID = {race['course']['courseID']}").execute()

                track = track.fetch_one()
                # print(track)
                race['course']['name'] = track[1]
                race['course']['starttime'] = race['starttime']
                final_results['getHorseRaces'].append(race)

            my_session.close()

            grey2_data = []
            over_25_data = []
            dutching_data = []
            print("add greyhound trades to races")
            for greyhound_race in final_results['getGreyhoundRaces']:
                trade = next(
                    (item for item in final_results['getSportsTrade'] if item["eventID"] == greyhound_race['_id']), None)
                # print('trade')
                # print(trade)
                if trade is not None:
                    greyhound_race['trade'] = trade
                grey2_data.append(greyhound_race)
            print('adding positions to football')
            for trade in final_results['getSportsTrade']:
                # print(trade)
                for position in trade['positions']:
                    if "Over 2.5" in position['strategy']:
                        over_25_data.append(trade)
                    elif "Dutching" in position['strategy']:
                        dutching_data.append(trade)

            print("sending to trade_data")
            # print(final_results['getHorseRaces'])
            task_info = {"task": "horserace_events",
                         'data': final_results['getHorseRaces']}
            dataQueue.put(json.dumps(task_info))
            task_info = {"task": "greyhound_events",
                         'data': final_results['getGreyhoundRaces']}
            dataQueue.put(json.dumps(task_info))
            task_info = {"task": "start_grey2", 'data': grey2_data}
            dataQueue.put(json.dumps(task_info))
            dataQueue.put(json.dumps(
                {"task": "start_over25", 'data': over_25_data}))
            dataQueue.put(json.dumps(
                {"task": "start_dutching", 'data': dutching_data}))
        except:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                            errorTitle: "Getting Initial Trading Data",
                            machine: "{ip_address}",
                            machineName: "API",
                            errorFileName: "{file}",
                            err: "{err},
                            critical: true
                        }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})


def main():

    manager = Manager()
    trade_data = manager.dict({
        'trades': {},
        'tradesToLog': {},
        "stakes": {}
    })

    print(trade_data)

    consumer = Process(target=dataTaskConsumer, args=(
        dataQueue, resultQueue, trade_data))
    consumer.start()

    # BankRoll = Process(target=BankRollController,
    # args=(trade_data, dataQueue))
    # BankRoll.start()
    asyncio.run(get_current_trades())
    sleep(5)
    Grey2Sheet = Process(target=Grey2SheetController,
                         args=(trade_data, dataQueue))
    Grey2Sheet.start()
    # Grey1Sheet = Process(target=Grey1SheetController,
    # args=(trade_data, dataQueue))
    # Grey1Sheet.start()

    # Over25 = Process(target=Over25SheetController,
    # args=(trade_data, dataQueue))
    # Over25.start()

    # Dutching = Process(target=DCSSheetController,
    #                   args=(trade_data, dataQueue))
    # Dutching.start()

    # Horses = Process(target=HorseSheetController,
    # args=(trade_data, dataQueue))
    # Horses.start()

    try:
        while True:
            print('processing...')
            sleep(60)
    except KeyboardInterrupt:
        print('!!Cleaning!!')
        consumer.join()
        # BankRoll.join()
        Grey2Sheet.join()
        # Grey1Sheet.join()
        # Over25.join()
        # Dutching.join()

    print("boo")


if __name__ == "__main__":
    dataQueue = JoinableQueue()
    resultQueue = Queue()
    main()
