import xlwings as xw
import mysqlx
from datetime import datetime, date, timedelta
import time
import traceback
import json
import os
import pytz
from time import sleep
from db import getGreyhoundRace
import requests, socket, traceback
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}

def Over25SheetController(trade_data, dataQueue):
    try:
        wb1 = xw.Book("over25_becky.xlsx")
        trading_sheet_becky = wb1.sheets['trading']
        results_sheet_becky = wb1.sheets['results']
        settings_sheet_becky = wb1.sheets['settings']
        wb2 = xw.Book("over25.xlsx")
        trading_sheet = wb2.sheets['trading']
        results_sheet = wb2.sheets['results']
        settings_sheet = wb2.sheets['settings']

        sheets = [{'trading': trading_sheet_becky, 'results': results_sheet_becky, 'settings': settings_sheet_becky, 'current_market_id': "", "current_settings_row": 0, "new_trades_logged": False}, {
            'trading': trading_sheet, 'results': results_sheet, 'settings': settings_sheet, 'current_market_id': "", "current_settings_row": 0, "new_trades_logged": False}]

        start_data = trade_data.get('over25Trades')

        for sheet in sheets:
            for index, trade in enumerate(start_data):
                print(trade)
                sheet['settings'][f'A{index+2}'].value = f"{trade['_id']}"
                sheet['settings'][f'B{index+2}'].value = f"{trade['eventID']}"
                sheet['settings'][f'C{index+2}'].value = f"{trade['event']}"
                sheet['settings'][f'D{index+2}'].value = trade['tradeDateTime']

        bet_refs = []
        running = True
        while running:
            for sheet in sheets:
                stakes = trade_data.get("stakes")
                if str(int(sheet['settings']['Q1'].value)) in stakes:
                    stakes = stakes[str(int(sheet['settings']['Q1'].value))]
                sheet['trading']['S1'].value = stakes['Over 2.5']
                trading_sheet = sheet['trading']
                settings_sheet = sheet['settings']
                if sheet['current_market_id'] != trading_sheet['N3'].value:
                    sheet['current_prices_logged'] = False
                    if sheet['results']['A2'].value not in bet_refs:
                        for i in range(2, 20):
                            if sheet['results'][f'A{i}'].value not in bet_refs and (sheet['results']['F2'].value == "RESULT_WON" or sheet['results']['F2'].value == "RESULT_LOST"):
                                stakes = trade_data.get("stakes")
                                stakes = stakes[str(
                                    int(sheet['settings']['Q1'].value))]
                                eventID = ""
                                tradeID = ""
                                starttime = 0
                                for x in range(2, 20):
                                    if settings_sheet[f"E{x}"].value == sheet['current_market_id']:
                                        eventID = settings_sheet[f"B{x}"].value
                                        tradeID = settings_sheet[f"A{x}"].value
                                        starttime = settings_sheet[f"D{x}"].value
                                bet_refs.append(sheet['results'][f'A{i}'].value)
                                trade = {
                                    'eventID': eventID,
                                    '_id': tradeID,
                                    'sport': "Football",
                                    'tradeDateTime': starttime,
                                    'positions': [{
                                        'runnerName': sheet['results'][f'B{i}'].value,
                                        'strategy': "Over 2.5 Before 20 Minutes",
                                        'positionType': "back",
                                        'odds': float(sheet['results'][f'E{i}'].value),
                                        'stake': stakes['Over 2.5'],
                                        "account": "Betfair",
                                        "liability": stakes['Over 2.5'],

                                    }],
                                    "userID": sheet['settings']['P1'].value,
                                    "sheet": sheet['settings']['P2'].value,
                                    "betID": sheet['results'][f'A{i}'].value
                                }
                                dataQueue.put(json.dumps(
                                    {'task': "add_trade_to_log", "data": trade}))
                            else:
                                break
                    sheet['current_market_id'] = trading_sheet['N3'].value
                secs_till_off = trading_sheet['D2'].value
                if secs_till_off <= 0.001388 and sheet['current_prices_logged'] is False:
                    print(trading_sheet['AA12'].value)
                    trading_sheet['X20'].value = trading_sheet['F6'].value

                    sheet['current_prices_logged'] = True
    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Trading Over 2.5 Strategy",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})