import requests, socket, traceback
from datetime import datetime, date, timedelta
API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}
def getGreyhoundRace(my_schema, event):
    try:
        collection = my_schema.get_collection("greyhound_races")
        print(event)
        event_array = event.split(" - ")
        date_array = event_array[0].split(" ")
        todays_date_string = date.today().strftime("%Y-%m-%d")
        track_name = " ".join(date_array[:len(date_array)-2])
        time_array = event_array[1].split(" ")
        event_url = f"https://www.timeform.com/greyhound-racing/racecards/{track_name.lower().replace(' ', '-')}/{time_array[0].replace(':', '')}/{todays_date_string}/"
        print(event_url)
        race = collection.find(f"url like '{event_url}%'").execute()

        race = race.fetch_one()

        return race
    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Greyhound Events",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)


        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})