from __future__ import print_function
import xlwings as xw
import asyncio
import logging
import mysqlx
from datetime import datetime, date, timedelta, timezone
import time
from gql import Client, gql
from gql.transport.websockets import WebsocketsTransport
from gql.transport.aiohttp import AIOHTTPTransport
from multiprocessing import Process, JoinableQueue, Queue, Manager, Lock
import traceback
import json
import os
import pytz
from time import sleep
from googleapiclient.discovery import build
import googleapiclient
from pprint import pprint
import requests, socket
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")



API_URL = "https://api.in4freedom.com/graphql"
API_KEY = "yEnWjCwwkanLlSdWNNldoSsdYdJS3uG9"
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}


def HorseSheetController(trade_data, dataQueue):
    try:
        wb1 = xw.Book("horses_becky.xlsx")
        trading_sheet_becky = wb1.sheets['trading']
        results_sheet_becky = wb1.sheets['results']
        settings_sheet_becky = wb1.sheets['settings']
        wb2 = xw.Book("horses.xlsx")
        trading_sheet = wb2.sheets['trading']
        results_sheet = wb2.sheets['results']
        settings_sheet = wb2.sheets['settings']

        start_data = trade_data.get('horseEvents')
        pprint(start_data)


        sheets = [{'trading': trading_sheet_becky, 'results': results_sheet_becky, 'settings': settings_sheet_becky, 'current_market_id': "", "current_settings_row": 0, "new_trades_logged": False}, {
            'trading': trading_sheet, 'results': results_sheet, 'settings': settings_sheet, 'current_market_id': "", "current_settings_row": 0, "new_trades_logged": False}]

        my_session = mysqlx.get_session({
            'host': DB_HOST, 'port': DB_PORT,
            'user': DB_USER, 'password': DB_PASS
        })

        my_schema = my_session.get_schema('in4freedom')

        collection = my_schema.get_collection("sports_trades")
        bet_refs = []
        running = True
        while running:
            for sheet in sheets:
                stakes = trade_data.get("stakes")
                #stakes = stakes[str(int(sheet['settings']['Q1'].value))]
                #sheet['trading']['S1'].value = stakes['Horses']
                sheet['trading']['S1'].value = 1
                trading_sheet = sheet['trading']
                settings_sheet = sheet['settings']
                if sheet['current_market_id'] != trading_sheet['N3'].value:
                    event_array = trading_sheet['A1'].value.split(" - ")
                    sheet['current_prices_logged'] = False
                    sheet['current_market_id'] = trading_sheet_becky['N3'].value
                    date_array = event_array[0].split(" ")
                    time_array = event_array[1].split(" ")
                    today = date.today()
                    naive = datetime.now()
                    timezone = pytz.timezone("Europe/London")
                    aware1 = timezone.localize(naive)
                    year = today.year
                    print(date_array)
                    start_time = int(datetime.timestamp(datetime.strptime(
                        f"{date_array[-2][:-2]}-{date_array[-1]}-{year} {time_array[0]}", "%d-%b-%Y %H:%M")))
                    track_name = " ".join(date_array[:len(date_array)-2])
                    print(start_time)
                    race = next(
                        (item for item in start_data if item["course"]['name'].lower().replace("ireland", "").strip() == track_name.lower() and item['starttime'] == start_time), None)
                    print(race)
                    try:
                        race_trades = collection.find(
                            f"eventID = '{race['_id']}'").execute()
                    except mysqlx.errors.InterfaceError:
                        my_session.close()
                        sleep(2)
                        my_session = mysqlx.get_session({
                            'host': DB_HOST, 'port': DB_PORT,
                            'user': DB_USER, 'password': DB_PASS
                        })

                        my_schema = my_session.get_schema('in4freedom')
                        collection = my_schema.get_collection("sports_trades")
                        race_trades = collection.find(
                            f"eventID = '{race['_id']}'").execute()

                    race_trades = race_trades.fetch_one()
                    print(race_trades)
                    settings_sheet['A2:E20'].clear_contents()

                    for index, position in enumerate(race_trades['positions']):
                        settings_sheet[f"A{index+2}"].value = position['runnerName']
                        settings_sheet[f"B{index+2}"].value = position['strategy']
                        settings_sheet[f"C{index+2}"].value = position['positionType']
                        settings_sheet[f"D{index+2}"].value = race_trades['eventID']
                        settings_sheet[f"E{index+2}"].value = race_trades['_id']
                        settings_sheet[f"E{index+2}"].value = race_trades['tradeDateTime']

                    if sheet['results']['A2'].value is not None and sheet['results']['A2'].value not in bet_refs:
                        for i in range(2, 20):
                            if sheet['results'][f'A{i}'].value not in bet_refs:
                                if current_event == "" or current_event is None:
                                    continue
                                if sheet['results'][f'B{i}'].value is None:
                                    continue
                                if sheet['results']['F2'].value != "RESULT_WON" and sheet['results']['F2'].value != "RESULT_LOST":
                                    continue

                                bet_refs.append(sheet['results'][f'A{i}'].value)
                                if sheet['results'][f'C{i}'].value == "B":
                                    if sheet['results'][f'F{i}'].value == "RESULT_WON":
                                        stake = float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1)
                                    else:
                                        stake = float(sheet['results'][f'E{i}'].value)
                                    liability = stake

                                else:
                                    if sheet['results'][f'F{i}'].value == "RESULT_WON":
                                        stake = float(sheet['results'][f'E{i}'].value)
                                        liability = float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1)
                                        
                                    else:
                                        stake = -1*(float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1))
                                        liability = -1*(float(sheet['results'][f'D{i}'].value)/(float(sheet['results'][f'E{i}'].value)-1))
                                print(sheet['results'][f'F{i}'].value)
                                trade = {
                                    'eventID': current_event['_id'],
                                    'sport': "Greyhounds",
                                    'tradeDateTime': current_event['starttime'],
                                    'positions': [{
                                        'runnerName': sheet['results'][f'B{i}'].value.split(" ", 1)[1],
                                        'strategy': "Greyhounds 2",
                                        'positionType': "back" if sheet['results'][f'C{i}'].value == "B" else "lay",
                                        'odds': float(sheet['results'][f'E{i}'].value),
                                        'stake': stake,
                                        "account": "Betfair",
                                        "liability": liability,
                                        "profit": 0.98*float(sheet['results'][f'D{i}'].value) if float(sheet['results'][f'D{i}'].value) > 0 else float(sheet['results'][f'D{i}'].value),
                                        "result": "win" if  sheet['results'][f'F{i}'].value == "RESULT_WON" else "loss"
                                    }],
                                    "userID": sheet['settings']['Q1'].value,
                                    "sheet": sheet['settings']['Q2'].value,
                                    "betID": sheet['results'][f'A{i}'].value
                                }
                                print("added trade")
                                dataQueue.put(json.dumps(
                                    {'task': "add_trade_to_log", "data": trade}))
                            else:
                                break
                    current_event = ""

                while True:
                    try:

                        secs_till_off = float(trading_sheet['D2'].value)*86400

                        if secs_till_off <= (600) and sheet['current_prices_logged'] is False:
                            print(trading_sheet['AA12'].value)
                            trading_sheet['AB15'].value = trading_sheet['AA15'].value
                            trading_sheet['AB16'].value = trading_sheet['AA16'].value
                            trading_sheet['AB17'].value = trading_sheet['AA17'].value


                            sheet['current_prices_logged'] = True
                        break
                    except ValueError:
                        sleep(2)
                        continue
                if sheet['current_prices_logged'] is True and trading_sheet['E2'].value == "In Play":
                    if trading_sheet['W1'].value is None:
                        trading_sheet['Q2'].value = -1
                    if trading_sheet['W1'].value == "DUTCHING":
                        total_stake = float(trading_sheet['W2'].value)
                        if (100/float(trading_sheet['W2'].value))*float(trading_sheet['Z5'].value) >= 25:
                            for i in range(5,50):
                                if sheet['trading'][f'A{i}'].value is None:
                                    break
                                sheet['trading'][f'Q{i}'].value = "CLOSE"
                    if trading_sheet['W1'].value == "CHOB":
                        for x in range(5, 50):
                            if sheet['trading'][f'A{x}'].value is None:
                                    break
                            if sheet['trading'][f'T{x}'].value is not None:
                                if (sheet['trading'][f'Q{x}'].value == "BACK" or sheet['trading'][f'Q{x}'].value == "UPDATE") and sheet['trading'][f'W{x}'].value > 0:
                                    backed_odds = float(sheet['trading'][f'T{x}'].value)
                                    
                                    if backed_odds < 2.5:
                                        lay_odds = backed_odds * 0.85
                                    elif backed_odds >= 2.5 and backed_odds < 4:
                                        lay_odds = backed_odds * 0.8
                                    else:
                                        lay_odds = backed_odds *0.75
                                    sheet['trading'][f'Q{x}'].value = "CLEAR"
                                    sleep(0.2)
                                    sheet['trading'][f'R{x}'].value = lay_odds
                                    sheet['trading'][f'Q{x}'].value = "LAY"
                                elif sheet['trading'][f'Q{x}'].value == "LAY" and sheet['trading'][f'W{x}'].value > 0:
                                    if backed_odds < 2.5 and (100/float(trading_sheet['W2'].value))*float(trading_sheet['Z5'].value) >= 20:
                                        sheet['trading'][f'Q{x}'].value = "CLEAR"
                                        sleep(0.2)
                                        sheet['trading'][f'Q{x}'].value = "CLOSE"
                                        sleep(1)
                                        trading_sheet['Q2'].value = -1
                                        sleep(1)
                                    elif backed_odds >= 2.5 and backed_odds < 4 and (100/float(trading_sheet['W2'].value))*float(trading_sheet['Z5'].value) >= 30:
                                        sheet['trading'][f'Q{x}'].value = "CLEAR"
                                        sleep(0.2)
                                        sheet['trading'][f'Q{x}'].value = "CLOSE"
                                        sleep(1)
                                        trading_sheet['Q2'].value = -1
                                        sleep(1)
                                    elif (100/float(trading_sheet['W2'].value))*float(trading_sheet['Z5'].value) >= 40:
                                        sheet['trading'][f'Q{x}'].value = "CLEAR"
                                        sleep(0.2)
                                        sheet['trading'][f'Q{x}'].value = "CLOSE"
                                        sleep(1)
                                        trading_sheet['Q2'].value = -1
                                        sleep(1)
                elif trading_sheet['E2'].value == "In Play":
                    trading_sheet['Q2'].value = -1
                    sleep(1)

                                


                                
                        

    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Trading Horses Strategy",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)
    

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
