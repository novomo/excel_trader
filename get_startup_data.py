from gql import Client
from gql.transport.aiohttp import AIOHTTPTransport
import os
from dotenv import load_dotenv
import mysqlx

# Environment Varibles
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")


API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",

    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

}


async def get_startup_data():

    transport = AIOHTTPTransport(
        url="https://api.in4freedom.com/graphql", headers=HEADERS, timeout=25)

    # Using `async with` on the client will start a connection on the transport
    # and provide a `session` variable to execute queries on this connection
    async with Client(
        transport=transport,
        fetch_schema_from_transport=True,
    ) as session:
        pass
