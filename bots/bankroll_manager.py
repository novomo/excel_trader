# Standard Python libraries
import os
from googleapiclient.discovery import build
from google.oauth2 import service_account
from dotenv import load_dotenv
from datetime import datetime
import json
# Environment Varibles
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

SHEET_ID = os.environ.get("SHEET_ID")
DIRECTORY = os.path.dirname(os.path.abspath(__file__))

SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/drive'
]


def get_next_trade_row(spreadsheet_service, sheet):
    range_name = f"'Trades'!O11:O"

    data = spreadsheet_service.spreadsheets().values().get(
        spreadsheetId=sheet, range=range_name).execute()
    data_rows = data.get('values', [])

    return len(data_rows) + 11, data_rows


def google_sheet_service():

    credentials = service_account.Credentials.from_service_account_file(
        f'{DIRECTORY}/client_secret.json', scopes=SCOPES)
    return build('sheets', 'v4', credentials=credentials)


def write_to_google_range(range_name, values, spreadsheet_service, sheet):

    value_input_option = 'USER_ENTERED'
    body = {
        'values': values
    }

    result = spreadsheet_service.spreadsheets().values().update(
        spreadsheetId=sheet, range=range_name,
        valueInputOption=value_input_option, body=body).execute()


def get_bankrolls_from_sheet(sheet, spreadsheet_service):
    range_name = f"'Accounts'!L2:O"

    data = spreadsheet_service.spreadsheets().values().get(
        spreadsheetId=sheet, range=range_name).execute()
    data_rows = data.get('values', [])
    bankrolls = []
    for row in data_rows:
        bankrolls.append({
            'strategy': row[0],
            'bankroll': float(row[1].replace("£", "")),
            'type': row[2],
            'unitSize': float(row[3].replace("£", "")),
        })
    return bankrolls


def get_trade_result_string(result):
    if result == "win":
        return "Y"
    else:
        return "N"


def get_trade_payout(position_type, result, stake, liability, odds):
    if position_type == "Lay":
        lay = "Y"
        if result == "Y":

            return stake+liability, lay
        else:

            return 0, lay

    else:
        lay = "N"
        if result == "Y":
            return stake*odds, lay

        else:
            return 0, lay


def change_units_on_sheet(new_bankroll_rows, sheet, spreadsheet_service):
    print(new_bankroll_rows)
    range_name = f"'Accounts'!O2:O"
    value_input_option = 'USER_ENTERED'
    body = {
        'values': new_bankroll_rows
    }

    result = spreadsheet_service.spreadsheets().values().update(
        spreadsheetId=sheet, range=range_name,
        valueInputOption=value_input_option, body=body).execute()
    # print('{0} cells updated.'.format(result.get('updatedCells')))


def bankroll_manager(trade_data, dataQueue):
    # define google sheet service
    spreadsheet_service = google_sheet_service()
    # get trades for trade_data
    trades = trade_data["tradesToLog"]
    stakes = trade_data["stakes"]  # is a list

    # add trades to google sheet
    next_row, on_sheet = get_next_trade_row(spreadsheet_service, SHEET_ID)
    trade_rows = []
    trades_to_delete = []
    todays_date = datetime.now().strftime("%Y-%m-%d")
    for key, trade in trades.items():
        print(key)
        print(on_sheet)
        logged = next(
            (item for item in on_sheet if int(item[0]) == int(float(key))), None)
        print("logged")
        print(logged)
        trades_to_delete.append(key)
        if logged is not None:
            continue
        result = get_trade_result_string(trade['result'])

        payout, lay = get_trade_payout(
            trade['positionType'], result, trade['stake'], trade['atRisk'], trade['odds'])

        trade_rows.append([
            trade['date'],
            trade['bookmaker'],
            trade['strategy'],
            trade['stake'],
            trade['odds'],
            trade['bonusBet'],
            trade['result'],
            trade['commission'],
            trade['layBet'],
            "",
            trade['atRisk'],
            trade['potemtialPayout'],
            trade['payout'],
            trade['pl'],
            f"{key}"])

    write_to_google_range(f"Trades!A{next_row}:P{next_row+len(trade_rows)}",
                          trade_rows, spreadsheet_service, SHEET_ID)

    # delete trades from tade_data
    dataQueue.put(json.dumps(
        {"task": "delete_trades_from_log", "data": trades_to_delete}))

    # get new bankroll data from google
    bankrolls = get_bankrolls_from_sheet(
        SHEET_ID, spreadsheet_service)
    new_stakes = {}
    new_unit_rows = []
    for bankroll in bankrolls:
        # calculate new unit size
        new_stakes[bankroll['strategy']] = {
            "bankroll": bankroll['bankroll'], 'strategy': bankroll['strategy']}
        try:
            if bankroll['type'] == "Kelly":
                new_unit_rows.append([float(bankroll['bankroll'])/280])
                new_stakes[bankroll['strategy']]['unitSize'] = float(
                    bankroll['bankroll'])/280
            elif bankroll['type'] == "Maria" and stakes[bankroll['strategy']]['bankroll'] > float(bankroll['bankroll']):
                new_unit_rows.append([float(bankroll['bankroll']/280)])
                new_stakes[bankroll['strategy']]['unitSize'] = float(
                    bankroll['bankroll'])/280
            else:
                new_unit_rows.append([float(bankroll['unitSize'])])
                new_stakes[bankroll['strategy']]['unitSize'] = float(
                    bankroll['unitSize'])
        except KeyError:
            new_unit_rows.append([float(bankroll['unitSize'])])
            new_stakes[bankroll['strategy']]['unitSize'] = float(
                bankroll['unitSize'])

    # change units on sheet
    change_units_on_sheet(new_unit_rows, SHEET_ID, spreadsheet_service)

    # change stakes property in trade_data
    dataQueue.put(json.dumps({"task": "updateStakes", "data": new_stakes}))
