
import xlwings as xw
from datetime import datetime
import json
from time import sleep


def uk_horse_place_dutching(trade_data, dataQueue):
    # open work book
    print(__file__.split("\\")[-1].replace('.py', ''))
    wb1 = xw.Book(f"{__file__.replace('py','')}xlsx")
    trading_sheet = wb1.sheets['trading']
    results_sheet = wb1.sheets['results']
    # get new trades
    last_trade = trade_data.get("lastTrades")
    print(last_trade)
    last_trade = last_trade[__file__.split("\\")[-1].replace('.py', '')]
    new_last_trade = ""
    for i in range(2, 10):
        if results_sheet[f'F{i}'].value != "RESULT_WON" and results_sheet[f'F{i}'].value != "RESULT_LOST":
            continue
        print(results_sheet[f'A{i}'].value)
        if results_sheet[f'A{i}'].value is None:
            break
        if i == 2:
            new_last_trade = results_sheet[f'A{i}'].value

        if float(results_sheet[f'D{i}'].value) < 0:
            stke = -1*float(results_sheet[f'D{i}'].value)
        else:
            stke = float(results_sheet[f'D{i}'].value) / \
                (float(results_sheet[f'E{i}'].value)-1)
        trade = {
            'date': datetime.now().strftime("%m/%d/%Y"),
            "bookmaker": "betfair",
            "strategy": "uk_horse_place_dutching",
            "stake": stke,
            "odds": float(results_sheet[f'E{i}'].value),
            "bonusBet": "N",
            "result": "Y" if results_sheet[f'F{i}'].value == "RESULT_WON" else "N",
            "commission": 0.02,
            "layBet": "N",
            "atRisk": stke,
            "positionType": "Back",
            'potemtialPayout': stke*(float(results_sheet[f'E{i}'].value)-1),
            'payout': 0 if results_sheet[f'D{i}'].value < 0 else stke*(float(results_sheet[f'E{i}'].value)-1),
            'pl': results_sheet[f'D{i}'].value if float(results_sheet[f'D{i}'].value) < 0 else results_sheet[f'D{i}'].value * 0.975,
            "betID": str(results_sheet[f'A{i}'].value)
        }
        # send to bank manager
        dataQueue.put(json.dumps(
            {'task': "add_trade_to_log", "data": trade}))
    if new_last_trade != "":
        dataQueue.put(json.dumps(
            {'task': "update_last_trade", "data": {"strategy": "uk_horse_place_dutching", "last_trade": new_last_trade}}))

    # update stake on sheet
    stakes = trade_data.get("stakes")
    print(stakes)
    if "uk_greys_place_dutch" in stakes:
        trading_sheet['S1'].value = stakes['uk_horse_place_dutching']['unitSize']
