# Standard Python libraries
import xlwings as xw
import asyncio
from multiprocessing import Process, JoinableQueue, Queue, Manager
import os
import traceback
import json
from time import sleep
from dotenv import load_dotenv
from python_import import importModuleByPath
from bots.bankroll_manager import bankroll_manager
from bots.uk_greys_place_dutch import uk_greys_place_dutch
from bots.aussie_greys_fav_dutching import aussie_greys_fav_dutching
from bots.uk_horse_place_dutching import uk_horse_place_dutching

# Other python files
from get_startup_data import get_startup_data


# Environment Varibles
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)


def delete_trades(data, trade_data):
    newTrades = trade_data.get('tradesToLog')
    # print(newTrades)
    # print(data)
    for tradeID in data:
        # print(tradeID)
        if tradeID in newTrades:
            del newTrades[tradeID]
    # print(newTrades)
    trade_data['tradesToLog'] = newTrades
    # print(trade_data['tradesToLog'])


def update_stakes(data, trade_data):
    trade_data['stakes'] = data


def changeLastTrades(data, trade_data):
    print(data)
    newTrades = trade_data.get('lastTrades')
    print(newTrades)
    newTrades[str(data['strategy'])] = data['last_trade']
    print(newTrades)
    trade_data['lastTrades'] = newTrades


def addTradeToLog(data, trade_data):
    newTrades = trade_data.get('tradesToLog')
    print(newTrades)
    newTrades[str(data['betID'])] = data
    trade_data['tradesToLog'] = newTrades


def data_task_consumer(queue, doneQueue, trade_data):
    while True:
        nextTask = queue.get()
        if nextTask == 'finished':
            doneQueue.put('finished')
            break
        task_info = json.loads(nextTask)

        if task_info['task'] == "delete_trades_from_log":
            # print(task_info)
            delete_trades(task_info['data'], trade_data)
        elif task_info['task'] == "updateStakes":
            update_stakes(task_info['data'], trade_data)
        elif task_info['task'] == "add_trade_to_log":
            addTradeToLog(task_info['data'], trade_data)
        elif task_info['task'] == "update_last_trade":
            changeLastTrades(task_info['data'], trade_data)
        queue.task_done()


def main():
    manager = Manager()

    # asyncio.run(get_startup_data())
    sleep(5)

    bots = []
    lastTrades = {}
    for subdir, dirs, files in os.walk(f"{os.path.dirname(os.path.abspath(__file__))}/bots"):
        for file in files:
            filename = os.fsdecode(file)
            if filename.endswith(".py"):
                print(os.path.join(subdir, file))
                imp = importModuleByPath(
                    os.path.join(subdir, file))
                func = getattr(imp, f'{filename.replace(".py", "")}')
                bots.append(func)
                lastTrades[filename.replace(".py", "")] = ""
    processes = []

    trade_data = manager.dict({
        'trades': {},
        'tradesToLog': {},
        "stakes": {},
        'lastTrades': lastTrades
    })
    consumer = Process(target=data_task_consumer, args=(
        dataQueue, resultQueue, trade_data))
    consumer.start()

    try:
        while True:
            bankroll_process = Process(
                target=bankroll_manager, args=(trade_data, dataQueue))
            uk_greys_place_dutch_process = Process(
                target=uk_greys_place_dutch, args=(trade_data, dataQueue))
            aussie_greys_fav_dutching_process = Process(
                target=aussie_greys_fav_dutching, args=(trade_data, dataQueue))
            uk_horse_place_dutching_process = Process(
                target=uk_horse_place_dutching, args=(trade_data, dataQueue))
            bankroll_process.start()
            uk_greys_place_dutch_process.start()
            aussie_greys_fav_dutching_process.start()
            uk_horse_place_dutching_process.start()

            bankroll_process.join()
            uk_greys_place_dutch_process.join()
            aussie_greys_fav_dutching_process.join()
            uk_horse_place_dutching_process.join()
            bankroll_process.close()
            uk_greys_place_dutch_process.close()
            aussie_greys_fav_dutching_process.close()
            uk_horse_place_dutching_process.close()
            sleep(5)
    except:
        traceback.print_exc()
    finally:
        for process in processes:
            process.join()

        consumer.join()


if __name__ == "__main__":
    dataQueue = JoinableQueue()
    resultQueue = Queue()
    main()
