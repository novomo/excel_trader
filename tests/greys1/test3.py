import pandas as pd
import os
import json
import matplotlib.pyplot as plt


def cal_lay_odds(odds):
    if odds < 2:
        return odds + 0.02
    elif odds < 3:
        return odds + 0.04
    elif odds < 4:
        return odds + 0.1
    elif odds < 6:
        return odds + 0.2
    else:
        return odds + 1


def calculate_p_l(selection_odds, risk):
    odds_1 = cal_lay_odds(selection_odds[0])
    odds_2 = cal_lay_odds(selection_odds[1])
    commission = 0.02
    lay_book = 100/odds_1+100/odds_2
    # print(lay_book)
    payout = ((risk/len(selection_odds))*100)/lay_book
    stake1 = payout/odds_1*(1-commission)
    stake2 = payout/odds_2*(1-commission)
    # print(stake1, stake2)
    risk = stake1*(odds_1-1)-stake2
    # print(risk)
    return payout*(1-commission), risk


profit, risk = calculate_p_l([3.3, 3.15], 4.21)

print(profit, risk)

BANK = 100
# STAKE = BANK/100


def uk_bet(odds_1, odds_2, odds_3, event_name, no_runners):
    if no_runners != 6:
        return False
    if " or" in event_name.lower():
        return False

    if odds_2 - odds_1 <= 0.4:
        return False
    if odds_3 - odds_1 >= 2:
        return False

    return True


def aus_bet(odds_1, odds_2, event_name, no_runners):
    if no_runners != 8:
        return False

    if odds_2 - odds_1 <= 1.5:
        return False
    if odds_2 - odds_1 >= 2.2:
        return False

    return True


PROFIT_DICT = {
    "aus8": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "auslay": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "uksec": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "uksecfav": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ukfav": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ukfav2": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ukdutch": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ausdutch": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    }
}

STAKE = 1


CURRENT_EVENT_ID = ""
CURRENT_FAV_1 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_2 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_3 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_4 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_5 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_6 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_7 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_8 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_NO_OF_RUNNERS = 0
CURRENT_DF = None
CURRENT_TRADE_VOL = None

WINNING_TRAPS = {}

CURRENT_TRACK = None

files = os.listdir(os.path.dirname(os.path.abspath(__file__)))

print(files)

GRAPH = {'x': [1], 'y': [BANK]}
BETS = 1
NO_RACES = 0
for file in files:
    if ".csv" in file and "win" in file:
        try:
            data = pd.read_csv(file, header=0)
        except pd.errors.ParserError:
            continue
        data.sort_values('EVENT_ID', inplace=True)
        dfs = [g for i, g in data.groupby(
            data['EVENT_ID'].ne(data['EVENT_ID'].shift()).cumsum())]

        for df in dfs:

            NO_RACES = NO_RACES+1
            CURRENT_TRACK = df.iloc[0]['MENU_HINT']
            CURRENT_NO_OF_RUNNERS = len(df.index)
            CURRENT_TRADE_VOL = df['PPTRADEDVOL'].sum()
            df.sort_values('BSP', inplace=True)
            first = df.iloc[0]
            CURRENT_FAV_1 = {'selectionId': first['SELECTION_ID'],
                             'odds': first['BSP'], 'winner': True if first['WIN_LOSE'] == 1 else False, "weightedAP": first['PPWAP'], "amountTraded": (
                100/CURRENT_TRADE_VOL)*first['PPTRADEDVOL'], 'trap': int(first['SELECTION_NAME'].split('.')[0])}
            try:
                second = df.iloc[1]

                CURRENT_FAV_2 = {'selectionId': second['SELECTION_ID'],
                                 'odds': second['BSP'], 'winner': True if second['WIN_LOSE'] == 1 else False, "weightedAP": second['PPWAP'], "amountTraded": (
                    100/CURRENT_TRADE_VOL)*second['PPTRADEDVOL'], 'trap': int(second['SELECTION_NAME'].split('.')[0])}
            except IndexError:
                continue
            try:
                third = df.iloc[2]
                CURRENT_FAV_3 = {'selectionId': third['SELECTION_ID'],
                                 'odds': third['BSP'], 'winner': True if third['WIN_LOSE'] == 1 else False, "weightedAP": third['PPWAP'], "amountTraded": (
                    100/CURRENT_TRADE_VOL)*third['PPTRADEDVOL'], 'trap': int(third['SELECTION_NAME'].split('.')[0])}
            except IndexError:
                continue
            try:
                fourth = df.iloc[3]
                CURRENT_FAV_4 = {'selectionId': fourth['SELECTION_ID'],
                                 'odds': fourth['BSP'], 'winner': True if fourth['WIN_LOSE'] == 1 else False, "weightedAP": fourth['PPWAP'], "amountTraded": (
                    100/CURRENT_TRADE_VOL)*third['PPTRADEDVOL'], 'trap': int(fourth['SELECTION_NAME'].split('.')[0])}
            except IndexError:
                continue
            try:
                fifth = df.iloc[4]
                CURRENT_FAV_5 = {'selectionId': fifth['SELECTION_ID'],
                                 'odds': fifth['BSP'], 'winner': True if fifth['WIN_LOSE'] == 1 else False, "weightedAP": fifth['PPWAP'], "amountTraded": (
                    100/CURRENT_TRADE_VOL)*fifth['PPTRADEDVOL'], 'trap': int(fifth['SELECTION_NAME'].split('.')[0])}
            except IndexError:
                continue

            if "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_NO_OF_RUNNERS == 8 and CURRENT_FAV_1['amountTraded'] < 50 and CURRENT_FAV_1['odds'] > 3 and (CURRENT_FAV_1['odds'] + 1.5) > CURRENT_FAV_2['odds']:
                PROFIT_DICT['aus8']['stake'] = PROFIT_DICT['aus8']['stake'] + STAKE
                PROFIT_DICT['aus8']['bets'] = PROFIT_DICT['aus8']['bets'] + 1
                percentages = []

                if CURRENT_FAV_1['winner'] is True or CURRENT_FAV_2['winner'] is True:
                    for index, row in df.iterrows():
                        if CURRENT_FAV_1['selectionId'] == row['SELECTION_ID'] or CURRENT_FAV_2['selectionId'] == row['SELECTION_ID']:
                            percentages.append(
                                100/(1+row['BSP']))
                            if row['WIN_LOSE'] == 1:
                                winnerPercent = 100 / \
                                    (1+row['BSP'])
                                winnerOdds = row['BSP']
                    if len(percentages) == 0:
                        continue
                    totalPercentages = sum(percentages)

                    winningsStake = STAKE * winnerPercent/totalPercentages
                    losingStake = STAKE - winningsStake
                    profit = ((winningsStake*winnerOdds) -
                              winningsStake-losingStake) * 0.98
                    PROFIT_DICT['aus8']['profit'] = PROFIT_DICT['aus8']['profit'] + profit
                    PROFIT_DICT['aus8']['wins'] = PROFIT_DICT['aus8']['wins'] + 1
                    BANK = BANK + profit

                else:
                    loss = STAKE
                    PROFIT_DICT['aus8']['profit'] = PROFIT_DICT['aus8']['profit'] - loss
                    BANK = BANK - loss
                BETS = BETS + 1
                STAKE = BANK/100
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)

print(PROFIT_DICT)
PROFIT_DICT['aus8']['roi'] = (
    100/PROFIT_DICT['aus8']['stake'])*PROFIT_DICT['aus8']['profit']


print(PROFIT_DICT)
print(BANK)
print(NO_RACES)
print(BETS)
# display scatter plot data
plt.figure(figsize=(10, 8))
plt.title('Scatter Plot', fontsize=20)
plt.xlabel('x', fontsize=15)
plt.ylabel('y', fontsize=15)
plt.scatter(GRAPH["x"], GRAPH["y"], marker='o')
plt.show()
