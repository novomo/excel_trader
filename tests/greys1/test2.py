import pandas as pd
import os
import json
import matplotlib.pyplot as plt


def cal_lay_odds(odds):
    if odds < 2:
        return odds + 0.02
    elif odds < 3:
        return odds + 0.04
    elif odds < 4:
        return odds + 0.1
    elif odds < 6:
        return odds + 0.2
    else:
        return odds + 1


def calculate_p_l(selection_odds, risk):
    odds_1 = cal_lay_odds(selection_odds[0])
    odds_2 = cal_lay_odds(selection_odds[1])
    commission = 0.02
    lay_book = 100/odds_1+100/odds_2
    # print(lay_book)
    payout = ((risk/len(selection_odds))*100)/lay_book
    stake1 = payout/odds_1*(1-commission)
    stake2 = payout/odds_2*(1-commission)
    # print(stake1, stake2)
    risk = stake1*(odds_1-1)-stake2
    # print(risk)
    return payout*(1-commission), risk


profit, risk = calculate_p_l([3.3, 3.15], 4.21)

print(profit, risk)

BANK = 100
# STAKE = BANK/100


def uk_bet(odds_1, odds_2, odds_3, event_name, no_runners):
    if no_runners != 6:
        return False
    if " or" in event_name.lower():
        return False

    if odds_2 - odds_1 <= 0.4:
        return False
    if odds_3 - odds_1 >= 2:
        return False

    return True


def aus_bet(odds_1, odds_2, event_name, no_runners):
    if no_runners != 8:
        return False

    if odds_2 - odds_1 <= 1.5:
        return False
    if odds_2 - odds_1 >= 2.2:
        return False

    return True


PROFIT_DICT = {
    "aus8": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "auslay": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "uksec": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "uksecfav": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ukfav": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ukfav2": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ukdutch": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    },
    "ausdutch": {
        'bets': 0,
        'wins': 0,
        'profit': 0,
        'stake': 0
    }
}

STAKE = 1


CURRENT_EVENT_ID = ""
CURRENT_FAV_1 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_2 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_3 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_4 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_5 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_6 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_7 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_FAV_8 = {'selectionId': "", 'odds': 0,
                 'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
CURRENT_NO_OF_RUNNERS = 0
CURRENT_DF = None
CURRENT_TRADE_VOL = None

WINNING_TRAPS = {}

CURRENT_TRACK = None

files = os.listdir(os.path.dirname(os.path.abspath(__file__)))

print(files)

GRAPH = {'x': [1], 'y': [BANK]}
BETS = 1
NO_RACES = 0
for file in files:
    if ".csv" in file and "win" in file:
        try:
            data = pd.read_csv(file, header=0)
        except pd.errors.ParserError:
            continue
        data.sort_values('EVENT_ID', inplace=True)
        dfs = [g for i, g in data.groupby(
            data['EVENT_ID'].ne(data['EVENT_ID'].shift()).cumsum())]

        for df in dfs:

            NO_RACES = NO_RACES+1
            CURRENT_TRACK = df.iloc[0]['MENU_HINT']
            CURRENT_NO_OF_RUNNERS = len(df.index)
            CURRENT_TRADE_VOL = df['PPTRADEDVOL'].sum()
            df.sort_values('BSP', inplace=True)
            first = df.iloc[0]
            CURRENT_FAV_1 = {'selectionId': first['SELECTION_ID'],
                             'odds': first['BSP'], 'winner': True if first['WIN_LOSE'] == 1 else False, "weightedAP": first['PPWAP'], "amountTraded": (
                100/CURRENT_TRADE_VOL)*first['PPTRADEDVOL'], 'trap': int(first['SELECTION_NAME'].split('.')[0])}
            try:
                second = df.iloc[1]

                CURRENT_FAV_2 = {'selectionId': second['SELECTION_ID'],
                                 'odds': second['BSP'], 'winner': True if second['WIN_LOSE'] == 1 else False, "weightedAP": second['PPWAP'], "amountTraded": (
                    100/CURRENT_TRADE_VOL)*second['PPTRADEDVOL'], 'trap': int(second['SELECTION_NAME'].split('.')[0])}
            except IndexError:
                continue
            try:
                third = df.iloc[2]
                CURRENT_FAV_3 = {'selectionId': third['SELECTION_ID'],
                                 'odds': third['BSP'], 'winner': True if third['WIN_LOSE'] == 1 else False, "weightedAP": third['PPWAP'], "amountTraded": (
                    100/CURRENT_TRADE_VOL)*third['PPTRADEDVOL'], 'trap': int(third['SELECTION_NAME'].split('.')[0])}
            except IndexError:
                continue
            if "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_NO_OF_RUNNERS == 8 and CURRENT_FAV_1['odds'] < 2 and CURRENT_FAV_1['odds'] > 1.75 and CURRENT_FAV_1['amountTraded'] > 65:
                PROFIT_DICT['aus8']['stake'] = PROFIT_DICT['aus8']['stake'] + STAKE
                PROFIT_DICT['aus8']['bets'] = PROFIT_DICT['aus8']['bets'] + 1
                if CURRENT_FAV_1['winner'] is True:
                    profit = ((CURRENT_FAV_1['odds'] - 1) * STAKE)*0.98
                    PROFIT_DICT['aus8']['profit'] = PROFIT_DICT['aus8']['profit'] + profit
                    PROFIT_DICT['aus8']['wins'] = PROFIT_DICT['aus8']['wins'] + 1
                    BANK = BANK + profit
                else:
                    loss = STAKE
                    PROFIT_DICT['aus8']['profit'] = PROFIT_DICT['aus8']['profit'] - loss
                    BANK = BANK - loss
                BETS = BETS + 1
                STAKE = BANK/100
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)
            elif "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_NO_OF_RUNNERS == 8 and CURRENT_FAV_1['odds'] < 3 and CURRENT_FAV_2['amountTraded'] > CURRENT_FAV_1['amountTraded']+15:
                PROFIT_DICT['auslay']['stake'] = PROFIT_DICT['auslay']['stake'] + STAKE
                PROFIT_DICT['auslay']['bets'] = PROFIT_DICT['auslay']['bets'] + 1
                if CURRENT_FAV_1['winner'] is False:
                    profit = STAKE * 0.98
                    PROFIT_DICT['auslay']['profit'] = PROFIT_DICT['auslay']['profit'] + profit
                    PROFIT_DICT['auslay']['wins'] = PROFIT_DICT['auslay']['wins'] + 1
                    BANK = BANK + profit
                else:
                    loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                    PROFIT_DICT['auslay']['profit'] = PROFIT_DICT['auslay']['profit'] - loss
                    BANK = BANK - loss
                STAKE = BANK/100
                BETS = BETS + 1
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)
            elif "(AUS)" not in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] < 2 and CURRENT_FAV_1['odds'] > 1.80 and CURRENT_NO_OF_RUNNERS == 5 and CURRENT_FAV_1['amountTraded'] > 65 and "OR" not in df.iloc(0)[0]['EVENT_NAME']:
                PROFIT_DICT['ukfav']['stake'] = PROFIT_DICT['ukfav']['stake'] + STAKE
                PROFIT_DICT['ukfav']['bets'] = PROFIT_DICT['ukfav']['bets'] + 1
                if CURRENT_FAV_1['winner'] is True:
                    profit = ((CURRENT_FAV_1['odds'] - 1) * STAKE)*0.98
                    PROFIT_DICT['ukfav']['profit'] = PROFIT_DICT['ukfav']['profit'] + profit
                    PROFIT_DICT['ukfav']['wins'] = PROFIT_DICT['ukfav']['wins'] + 1
                    BANK = BANK + profit
                else:
                    loss = STAKE

                    PROFIT_DICT['ukfav']['profit'] = PROFIT_DICT['ukfav']['profit'] - loss
                    BANK = BANK - loss
                STAKE = BANK/100
                BETS = BETS + 1
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)

            elif "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] < 4 and CURRENT_FAV_2['amountTraded'] > CURRENT_FAV_1['amountTraded']+25:
                PROFIT_DICT['uksecfav']['stake'] = PROFIT_DICT['uksecfav']['stake'] + STAKE
                PROFIT_DICT['uksecfav']['bets'] = PROFIT_DICT['uksecfav']['bets'] + 1
                if CURRENT_FAV_1['winner'] is False:
                    profit = STAKE * 0.98
                    PROFIT_DICT['uksecfav']['profit'] = PROFIT_DICT['uksecfav']['profit'] + profit
                    PROFIT_DICT['uksecfav']['wins'] = PROFIT_DICT['uksecfav']['wins'] + 1
                    BANK = BANK + profit
                else:
                    loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                    PROFIT_DICT['uksecfav']['profit'] = PROFIT_DICT['uksecfav']['profit'] - loss
                    BANK = BANK - loss
                STAKE = BANK/100
                BETS = BETS + 1
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)
            elif "(AUS)" not in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] > 2 and CURRENT_FAV_2['odds'] < 3 and CURRENT_FAV_1['amountTraded'] < 55 and ("A1" in df.iloc(0)[0]['EVENT_NAME'] or "A2" in df.iloc(0)[0]['EVENT_NAME'] or "S " in df.iloc(0)[0]['EVENT_NAME'] or "A3" in df.iloc(0)[0]['EVENT_NAME'] or "A4" in df.iloc(0)[0]['EVENT_NAME']):
                PROFIT_DICT['uksec']['bets'] = PROFIT_DICT['uksec']['bets'] + 1
                PROFIT_DICT['uksec']['stake'] = PROFIT_DICT['uksec']['stake'] + STAKE
                if CURRENT_FAV_1['winner'] is False:
                    profit = STAKE * 0.98
                    PROFIT_DICT['uksec']['profit'] = PROFIT_DICT['uksec']['profit'] + profit
                    PROFIT_DICT['uksec']['wins'] = PROFIT_DICT['uksec']['wins'] + 1
                    BANK = BANK + profit
                else:
                    loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                    PROFIT_DICT['uksec']['profit'] = PROFIT_DICT['uksec']['profit'] - loss
                    BANK = BANK - loss
                STAKE = BANK/100
                BETS = BETS + 1
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)
            elif "(AUS)" not in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] + 0.4 > CURRENT_FAV_3['odds'] and CURRENT_FAV_1['amountTraded'] < 55 and ("A1" in df.iloc(0)[0]['EVENT_NAME'] or "A2" in df.iloc(0)[0]['EVENT_NAME'] or "S " in df.iloc(0)[0]['EVENT_NAME'] or "A3" in df.iloc(0)[0]['EVENT_NAME'] or "A4" in df.iloc(0)[0]['EVENT_NAME']):
                PROFIT_DICT['ukfav2']['stake'] = PROFIT_DICT['ukfav2']['stake'] + STAKE
                PROFIT_DICT['ukfav2']['bets'] = PROFIT_DICT['ukfav2']['bets'] + 1
                if CURRENT_FAV_1['winner'] is False:
                    profit = STAKE * 0.98
                    PROFIT_DICT['ukfav2']['profit'] = PROFIT_DICT['ukfav2']['profit'] + profit
                    PROFIT_DICT['ukfav2']['wins'] = PROFIT_DICT['ukfav2']['wins'] + 1
                else:
                    loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                    PROFIT_DICT['ukfav2']['profit'] = PROFIT_DICT['ukfav2']['profit'] - loss
                    BANK = BANK - loss
                BANK = BANK + profit
                STAKE = BANK/100
                BETS = BETS + 1
                GRAPH['x'].append(BETS)
                GRAPH['y'].append(BANK)
            WINNER = df.loc[(
                df['WIN_LOSE'] == 1)]
            print(WINNER)

            try:
                int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0])
            except IndexError:
                continue

            if CURRENT_TRACK in WINNING_TRAPS:
                if len(WINNING_TRAPS[CURRENT_TRACK]) > 0:
                    if "(AUS)" not in df.iloc(0)[0]['MENU_HINT']:

                        trend = sum(
                            i > 3 for i in WINNING_TRAPS[CURRENT_TRACK])
                        per = (100/len(WINNING_TRAPS[CURRENT_TRACK]))*trend
                        percentages = []
                        if per > 68:
                            PROFIT_DICT['ukdutch']['stake'] = PROFIT_DICT['ukdutch']['stake'] + STAKE
                            PROFIT_DICT['ukdutch']['bets'] = PROFIT_DICT['ukdutch']['bets'] + 1
                            try:
                                if int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0]) < 4:
                                    loss = STAKE
                                    PROFIT_DICT['ukdutch']['profit'] = PROFIT_DICT['ukdutch']['profit'] - loss
                                    BANK = BANK - loss
                                else:

                                    for index, row in df.iterrows():
                                        if "4." in row['SELECTION_NAME'] or "5." in row['SELECTION_NAME'] or "6." in row['SELECTION_NAME']:
                                            percentages.append(
                                                100/(1+row['BSP']))
                                            if row['WIN_LOSE'] == 1:
                                                winnerPercent = 100 / \
                                                    (1+row['BSP'])
                                                winnerOdds = row['BSP']
                                    if len(percentages) == 0:
                                        continue
                                    totalPercentages = sum(percentages)

                                    winningsStake = STAKE * winnerPercent/totalPercentages
                                    losingStake = STAKE - winningsStake
                                    profit = ((winningsStake*winnerOdds) -
                                              winningsStake-losingStake) * 0.98
                                    PROFIT_DICT['ukdutch']['profit'] = PROFIT_DICT['ukdutch']['profit'] + profit
                                    PROFIT_DICT['ukdutch']['wins'] = PROFIT_DICT['ukdutch']['wins'] + 1
                                    BANK = BANK + profit
                            except IndexError:
                                continue

                        elif per < 32:
                            PROFIT_DICT['ukdutch']['stake'] = PROFIT_DICT['ukdutch']['stake'] + STAKE
                            PROFIT_DICT['ukdutch']['bets'] = PROFIT_DICT['ukdutch']['bets'] + 1
                            if int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0]) > 3:
                                loss = STAKE
                                PROFIT_DICT['ukdutch']['profit'] = PROFIT_DICT['ukdutch']['profit'] - loss
                                BANK = BANK - loss
                            else:

                                for index, row in df.iterrows():
                                    if "3." in row['SELECTION_NAME'] or "2." in row['SELECTION_NAME'] or "1." in row['SELECTION_NAME']:
                                        percentages.append(100/(1+row['BSP']))
                                        if row['WIN_LOSE'] == 1:
                                            winnerPercent = 100/(1+row['BSP'])
                                            winnerOdds = row['BSP']
                                if len(percentages) == 0:
                                    continue
                                totalPercentages = sum(percentages)

                                winningsStake = STAKE * winnerPercent/totalPercentages
                                losingStake = STAKE - winningsStake
                                profit = ((winningsStake*winnerOdds) -
                                          winningsStake-losingStake) * 0.98
                                PROFIT_DICT['ukdutch']['profit'] = PROFIT_DICT['ukdutch']['profit'] + profit
                                PROFIT_DICT['ukdutch']['wins'] = PROFIT_DICT['ukdutch']['wins'] + 1
                                BANK = BANK + profit
                    if "(AUS)" in df.iloc(0)[0]['MENU_HINT']:

                        trend = sum(
                            i > 3 for i in WINNING_TRAPS[CURRENT_TRACK])
                        per = (100/len(WINNING_TRAPS[CURRENT_TRACK]))*trend
                        percentages = []
                        if per > 74:
                            PROFIT_DICT['ausdutch']['stake'] = PROFIT_DICT['ausdutch']['stake'] + STAKE
                            PROFIT_DICT['ausdutch']['bets'] = PROFIT_DICT['ausdutch']['bets'] + 1
                            try:
                                if int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0]) < 5:
                                    loss = STAKE
                                    PROFIT_DICT['ausdutch']['profit'] = PROFIT_DICT['ausdutch']['profit'] - loss
                                    BANK = BANK - loss
                                else:

                                    for index, row in df.iterrows():
                                        if "5." in row['SELECTION_NAME'] or "6." in row['SELECTION_NAME'] or "7." in row['SELECTION_NAME'] or "8." in row['SELECTION_NAME']:
                                            percentages.append(
                                                100/(1+row['BSP']))
                                            if row['WIN_LOSE'] == 1:
                                                winnerPercent = 100 / \
                                                    (1+row['BSP'])
                                                winnerOdds = row['BSP']
                                    if len(percentages) == 0:
                                        continue
                                    totalPercentages = sum(percentages)

                                    winningsStake = STAKE * winnerPercent/totalPercentages
                                    losingStake = STAKE - winningsStake
                                    profit = ((winningsStake*winnerOdds) -
                                              winningsStake-losingStake) * 0.98
                                    PROFIT_DICT['ausdutch']['profit'] = PROFIT_DICT['ausdutch']['profit'] + profit
                                    PROFIT_DICT['ausdutch']['wins'] = PROFIT_DICT['ausdutch']['wins'] + 1
                                    BANK = BANK + profit
                            except IndexError:
                                continue

                        elif per < 26:
                            PROFIT_DICT['ausdutch']['stake'] = PROFIT_DICT['ausdutch']['stake'] + STAKE
                            PROFIT_DICT['ausdutch']['bets'] = PROFIT_DICT['ausdutch']['bets'] + 1
                            if int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0]) > 4:
                                loss = STAKE
                                PROFIT_DICT['ausdutch']['profit'] = PROFIT_DICT['ausdutch']['profit'] - loss
                                BANK = BANK - loss
                            else:

                                for index, row in df.iterrows():
                                    if "4." in row['SELECTION_NAME'] or "3." in row['SELECTION_NAME'] or "2." in row['SELECTION_NAME'] or "1." in row['SELECTION_NAME']:
                                        percentages.append(100/(1+row['BSP']))
                                        if row['WIN_LOSE'] == 1:
                                            winnerPercent = 100/(1+row['BSP'])
                                            winnerOdds = row['BSP']
                                if len(percentages) == 0:
                                    continue
                                totalPercentages = sum(percentages)

                                winningsStake = STAKE * winnerPercent/totalPercentages
                                losingStake = STAKE - winningsStake
                                profit = ((winningsStake*winnerOdds) -
                                          winningsStake-losingStake) * 0.98
                                PROFIT_DICT['ausdutch']['profit'] = PROFIT_DICT['ausdutch']['profit'] + profit
                                PROFIT_DICT['ausdutch']['wins'] = PROFIT_DICT['ausdutch']['wins'] + 1
                                BANK = BANK + profit

                    STAKE = BANK/100
                try:
                    WINNING_TRAPS[CURRENT_TRACK].append(
                        int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0]))
                except IndexError:
                    continue
            else:
                try:
                    WINNING_TRAPS[CURRENT_TRACK] = [
                        int(WINNER.iloc[0]['SELECTION_NAME'].split(".")[0])]
                except IndexError:
                    continue
PROFIT_DICT['aus8']['roi'] = (
    100/PROFIT_DICT['aus8']['stake'])*PROFIT_DICT['aus8']['profit']
PROFIT_DICT['auslay']['roi'] = (
    100/PROFIT_DICT['auslay']['stake'])*PROFIT_DICT['auslay']['profit']
PROFIT_DICT['uksec']['roi'] = (
    100/PROFIT_DICT['uksec']['stake'])*PROFIT_DICT['uksec']['profit']
PROFIT_DICT['uksecfav']['roi'] = (
    100/PROFIT_DICT['uksecfav']['stake'])*PROFIT_DICT['uksecfav']['profit']
PROFIT_DICT['ukfav']['roi'] = (
    100/PROFIT_DICT['ukfav']['stake'])*PROFIT_DICT['ukfav']['profit']
PROFIT_DICT['ukfav2']['roi'] = (
    100/PROFIT_DICT['ukfav2']['stake'])*PROFIT_DICT['ukfav2']['profit']
PROFIT_DICT['ukdutch']['roi'] = (
    100/PROFIT_DICT['ukdutch']['stake'])*PROFIT_DICT['ukdutch']['profit']
PROFIT_DICT['ausdutch']['roi'] = (
    100/PROFIT_DICT['ausdutch']['stake'])*PROFIT_DICT['ausdutch']['profit']

print(PROFIT_DICT)
print(BANK)
print(NO_RACES)
print(BETS)
# display scatter plot data
plt.figure(figsize=(10, 8))
plt.title('Scatter Plot', fontsize=20)
plt.xlabel('x', fontsize=15)
plt.ylabel('y', fontsize=15)
plt.scatter(GRAPH["x"], GRAPH["y"], marker='o')
plt.show()
'''
        for i, df.iloc(0)[0] in data.iterdf.iloc(0)[0]s():
            if CURRENT_EVENT_ID != df.iloc(0)[0]['EVENT_ID']:
                try:

                    if "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_NO_OF_RUNNERS == 8 and CURRENT_FAV_1['odds'] < 2 and CURRENT_FAV_1['odds'] > 1.75 and CURRENT_FAV_1['amountTraded'] > 65:
                        PROFIT_DICT['aus8']['stake'] = PROFIT_DICT['aus8']['stake'] + STAKE
                        PROFIT_DICT['aus8']['bets'] = PROFIT_DICT['aus8']['bets'] + 1
                        if CURRENT_FAV_1['winner'] is True:
                            profit = ((CURRENT_FAV_1['odds'] - 1) * STAKE)*0.98
                            PROFIT_DICT['aus8']['profit'] = PROFIT_DICT['aus8']['profit'] + profit
                            PROFIT_DICT['aus8']['wins'] = PROFIT_DICT['aus8']['wins'] + 1
                            BANK = BANK + profit
                        else:
                            loss = STAKE
                            PROFIT_DICT['aus8']['profit'] = PROFIT_DICT['aus8']['profit'] - loss
                            BANK = BANK - loss
                        BETS = BETS + 1
                        STAKE = BANK/100
                        GRAPH['x'].append(BETS)
                        GRAPH['y'].append(BANK)
                    elif "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_NO_OF_RUNNERS == 8 and CURRENT_FAV_1['odds'] < 3 and CURRENT_FAV_2['amountTraded'] > CURRENT_FAV_1['amountTraded']+15:
                        PROFIT_DICT['auslay']['stake'] = PROFIT_DICT['auslay']['stake'] + STAKE
                        PROFIT_DICT['auslay']['bets'] = PROFIT_DICT['auslay']['bets'] + 1
                        if CURRENT_FAV_1['winner'] is False:
                            profit = STAKE * 0.98
                            PROFIT_DICT['auslay']['profit'] = PROFIT_DICT['auslay']['profit'] + profit
                            PROFIT_DICT['auslay']['wins'] = PROFIT_DICT['auslay']['wins'] + 1
                            BANK = BANK + profit
                        else:
                            loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                            PROFIT_DICT['auslay']['profit'] = PROFIT_DICT['auslay']['profit'] - loss
                            BANK = BANK - loss
                        STAKE = BANK/100
                        BETS = BETS + 1
                        GRAPH['x'].append(BETS)
                        GRAPH['y'].append(BANK)
                    elif "(AUS)" not in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] < 2 and CURRENT_FAV_1['odds'] > 1.80 and CURRENT_NO_OF_RUNNERS == 5 and CURRENT_FAV_1['amountTraded'] > 65 and "OR" not in df.iloc(0)[0]['EVENT_NAME']:
                        PROFIT_DICT['ukfav']['stake'] = PROFIT_DICT['ukfav']['stake'] + STAKE
                        PROFIT_DICT['ukfav']['bets'] = PROFIT_DICT['ukfav']['bets'] + 1
                        if CURRENT_FAV_1['winner'] is True:
                            profit = ((CURRENT_FAV_1['odds'] - 1) * STAKE)*0.98
                            PROFIT_DICT['ukfav']['profit'] = PROFIT_DICT['ukfav']['profit'] + profit
                            PROFIT_DICT['ukfav']['wins'] = PROFIT_DICT['ukfav']['wins'] + 1
                            BANK = BANK + profit
                        else:
                            loss = STAKE

                            PROFIT_DICT['ukfav']['profit'] = PROFIT_DICT['ukfav']['profit'] - loss
                            BANK = BANK - loss
                        STAKE = BANK/100
                        BETS = BETS + 1
                        GRAPH['x'].append(BETS)
                        GRAPH['y'].append(BANK)

                    elif "(AUS)" in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] < 4 and CURRENT_FAV_2['amountTraded'] > CURRENT_FAV_1['amountTraded']+25:
                        PROFIT_DICT['uksecfav']['stake'] = PROFIT_DICT['uksecfav']['stake'] + STAKE
                        PROFIT_DICT['uksecfav']['bets'] = PROFIT_DICT['uksecfav']['bets'] + 1
                        if CURRENT_FAV_1['winner'] is False:
                            profit = STAKE * 0.98
                            PROFIT_DICT['uksecfav']['profit'] = PROFIT_DICT['uksecfav']['profit'] + profit
                            PROFIT_DICT['uksecfav']['wins'] = PROFIT_DICT['uksecfav']['wins'] + 1
                            BANK = BANK + profit
                        else:
                            loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                            PROFIT_DICT['uksecfav']['profit'] = PROFIT_DICT['uksecfav']['profit'] - loss
                            BANK = BANK - loss
                        STAKE = BANK/100
                        BETS = BETS + 1
                        GRAPH['x'].append(BETS)
                        GRAPH['y'].append(BANK)
                    elif "(AUS)" not in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] > 2 and CURRENT_FAV_2['odds'] < 3 and CURRENT_FAV_1['amountTraded'] < 55 and ("A1" in df.iloc(0)[0]['EVENT_NAME'] or "A2" in df.iloc(0)[0]['EVENT_NAME'] or "S " in df.iloc(0)[0]['EVENT_NAME'] or "A3" in df.iloc(0)[0]['EVENT_NAME'] or "A4" in df.iloc(0)[0]['EVENT_NAME']):
                        PROFIT_DICT['uksec']['bets'] = PROFIT_DICT['uksec']['bets'] + 1
                        PROFIT_DICT['uksec']['stake'] = PROFIT_DICT['uksec']['stake'] + STAKE
                        if CURRENT_FAV_1['winner'] is False:
                            profit = STAKE * 0.98
                            PROFIT_DICT['uksec']['profit'] = PROFIT_DICT['uksec']['profit'] + profit
                            PROFIT_DICT['uksec']['wins'] = PROFIT_DICT['uksec']['wins'] + 1
                            BANK = BANK + profit
                        else:
                            loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                            PROFIT_DICT['uksec']['profit'] = PROFIT_DICT['uksec']['profit'] - loss
                            BANK = BANK - loss
                        STAKE = BANK/100
                        BETS = BETS + 1
                        GRAPH['x'].append(BETS)
                        GRAPH['y'].append(BANK)
                    elif "(AUS)" not in df.iloc(0)[0]['MENU_HINT'] and CURRENT_FAV_1['odds'] + 0.4 > CURRENT_FAV_3['odds'] and CURRENT_FAV_1['amountTraded'] < 55 and ("A1" in df.iloc(0)[0]['EVENT_NAME'] or "A2" in df.iloc(0)[0]['EVENT_NAME'] or "S " in df.iloc(0)[0]['EVENT_NAME'] or "A3" in df.iloc(0)[0]['EVENT_NAME'] or "A4" in df.iloc(0)[0]['EVENT_NAME']):
                        PROFIT_DICT['ukfav2']['stake'] = PROFIT_DICT['ukfav2']['stake'] + STAKE
                        PROFIT_DICT['ukfav2']['bets'] = PROFIT_DICT['ukfav2']['bets'] + 1
                        if CURRENT_FAV_1['winner'] is False:
                            profit = STAKE * 0.98
                            PROFIT_DICT['ukfav2']['profit'] = PROFIT_DICT['ukfav2']['profit'] + profit
                            PROFIT_DICT['ukfav2']['wins'] = PROFIT_DICT['ukfav2']['wins'] + 1
                        else:
                            loss = STAKE * (CURRENT_FAV_1['odds'] - 1)
                            PROFIT_DICT['ukfav2']['profit'] = PROFIT_DICT['ukfav2']['profit'] - loss
                            BANK = BANK - loss
                        BANK = BANK + profit
                        # STAKE = BANK/100
                        BETS = BETS + 1
                        GRAPH['x'].append(BETS)
                        GRAPH['y'].append(BANK)
                    if CURRENT_DF is not None:
                        print(CURRENT_DF)
                        winner = 0
                        for index, df.iloc(0)[0] in CURRENT_DF.iterdf.iloc(0)[0]s():
                            if df.iloc(0)[0]['WIN_LOSE'] is True:
                                winner = df.iloc(0)[0]['SELECTION_NAME'].split('.')[0]
                                break
                        if CURRENT_TRACK in WINNING_TRAPS:

                            WINNING_TRAPS[CURRENT_TRACK].append(winner)
                        else:
                            WINNING_TRAPS[CURRENT_TRACK] = [winner]
                        print(winner)

                    CURRENT_DF = None
                    CURRENT_TRADE_VOL = None
                    NO_RACES = NO_RACES + 1
                    # print(BANK)

                except KeyError:
                    pass
            if CURRENT_DF is None:
                try:
                    CURRENT_TRACK = df.iloc(0)[0]['MENU_HINT']
                    CURRENT_EVENT_ID = df.iloc(0)[0]['EVENT_ID']
                    CURRENT_DF = data.loc[(
                        data['EVENT_ID'] == df.iloc(0)[0]['EVENT_ID'])]

                    CURRENT_NO_OF_RUNNERS = len(CURRENT_DF.index)
                    CURRENT_TRADE_VOL = CURRENT_DF['PPTRADEDVOL'].sum()
                    CURRENT_DF.sort_values('BSP', inplace=True)

                    first = CURRENT_DF.iloc[0]
                    CURRENT_FAV_1 = {'selectionId': first['SELECTION_ID'],
                                     'odds': first['BSP'], 'winner': True if first['WIN_LOSE'] == 1 else False, "weightedAP": first['PPWAP'], "amountTraded": (
                        100/CURRENT_TRADE_VOL)*first['PPTRADEDVOL'], 'trap': int(first['SELECTION_NAME'].split('.')[0])}
                    second = CURRENT_DF.iloc[1]
                    CURRENT_FAV_2 = {'selectionId': second['SELECTION_ID'],
                                     'odds': second['BSP'], 'winner': True if second['WIN_LOSE'] == 1 else False, "weightedAP": second['PPWAP'], "amountTraded": (
                        100/CURRENT_TRADE_VOL)*second['PPTRADEDVOL'], 'trap': int(second['SELECTION_NAME'].split('.')[0])}
                    third = CURRENT_DF.iloc[2]
                    CURRENT_FAV_3 = {'selectionId': third['SELECTION_ID'],
                                     'odds': third['BSP'], 'winner': True if third['WIN_LOSE'] == 1 else False, "weightedAP": third['PPWAP'], "amountTraded": (
                        100/CURRENT_TRADE_VOL)*third['PPTRADEDVOL'], 'trap': int(third['SELECTION_NAME'].split('.')[0])}
                    fourth = CURRENT_DF.iloc[3]
                    CURRENT_FAV_4 = {'selectionId': first['SELECTION_ID'],
                                     'odds': fourth['BSP'], 'winner': True if fourth['WIN_LOSE'] == 1 else False, "weightedAP": fourth['PPWAP'], "amountTraded": (
                        100/CURRENT_TRADE_VOL)*fourth['PPTRADEDVOL'], 'trap': int(fourth['SELECTION_NAME'].split('.')[0])}
                    fifth = CURRENT_DF.iloc[4]
                    CURRENT_FAV_5 = {'selectionId': fifth['SELECTION_ID'],
                                     'odds': fifth['BSP'], 'winner': True if fifth['WIN_LOSE'] == 1 else False, "weightedAP": fifth['PPWAP'], "amountTraded": (
                        100/CURRENT_TRADE_VOL)*fifth['PPTRADEDVOL'], 'trap': int(fifth['SELECTION_NAME'].split('.')[0])}
                    if CURRENT_NO_OF_RUNNERS >= 6:
                        sixth = CURRENT_DF.iloc[5]
                        CURRENT_FAV_6 = {'selectionId': sixth['SELECTION_ID'],
                                         'odds': sixth['BSP'], 'winner': True if sixth['WIN_LOSE'] == 1 else False, "weightedAP": sixth['PPWAP'], "amountTraded": (
                            100/CURRENT_TRADE_VOL)*sixth['PPTRADEDVOL'], 'trap': int(sixth['SELECTION_NAME'].split('.')[0])}
                    if CURRENT_NO_OF_RUNNERS >= 7:
                        seventh = CURRENT_DF.iloc[6]
                        CURRENT_FAV_7 = {'selectionId': seventh['SELECTION_ID'],
                                         'odds': seventh['BSP'], 'winner': True if seventh['WIN_LOSE'] == 1 else False, "weightedAP": seventh['PPWAP'], "amountTraded": (
                            100/CURRENT_TRADE_VOL)*seventh['PPTRADEDVOL'], 'trap': int(seventh['SELECTION_NAME'].split('.')[0])}
                    if CURRENT_NO_OF_RUNNERS >= 8:
                        eighth = CURRENT_DF.iloc[7]
                        CURRENT_FAV_8 = {'selectionId': eighth['SELECTION_ID'],
                                         'odds': eighth['BSP'], 'winner': True if eighth['WIN_LOSE'] == 1 else False, "weightedAP": eighth['PPWAP'], "amountTraded": (
                            100/CURRENT_TRADE_VOL)*eighth['PPTRADEDVOL'], 'trap': int(first['SELECTION_NAME'].split('.')[0])}
                except IndexError:
                    CURRENT_EVENT_ID = df.iloc(0)[0]['EVENT_ID']
                    CURRENT_TRACK = None
                    CURRENT_FAV_1 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_2 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_3 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_4 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_5 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_6 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_7 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_FAV_8 = {'selectionId': "", 'odds': 0,
                                     'winner': False, "weightedAP": 0, "amountTraded": 0, 'trap': 0}
                    CURRENT_NO_OF_RUNNERS = 0
                    CURRENT_DF = None


PROFIT_DICT['aus8']['roi'] = (
    100/PROFIT_DICT['aus8']['stake'])*PROFIT_DICT['aus8']['profit']
PROFIT_DICT['auslay']['roi'] = (
    100/PROFIT_DICT['auslay']['stake'])*PROFIT_DICT['auslay']['profit']
PROFIT_DICT['uksec']['roi'] = (
    100/PROFIT_DICT['uksec']['stake'])*PROFIT_DICT['uksec']['profit']
PROFIT_DICT['uksecfav']['roi'] = (
    100/PROFIT_DICT['uksecfav']['stake'])*PROFIT_DICT['uksecfav']['profit']
PROFIT_DICT['ukfav']['roi'] = (
    100/PROFIT_DICT['ukfav']['stake'])*PROFIT_DICT['ukfav']['profit']
PROFIT_DICT['ukfav2']['roi'] = (
    100/PROFIT_DICT['ukfav2']['stake'])*PROFIT_DICT['ukfav2']['profit']

print(PROFIT_DICT)
print(BANK)
print(NO_RACES)
print(BETS)
print(WINNING_TRAPS)
# display scatter plot data
plt.figure(figsize=(10, 8))
plt.title('Scatter Plot', fontsize=20)
plt.xlabel('x', fontsize=15)
plt.ylabel('y', fontsize=15)
plt.scatter(GRAPH["x"], GRAPH["y"], marker='o')
plt.show()
'''
