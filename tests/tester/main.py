import os
import pandas as pd
import matplotlib.pyplot as plt
MARKETS = [
    {"GREYSHOUNDS_UK_PLACE": "greyhoundplace", "winners": 2}
]
# loop files
print(
    "/".join([os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "greys1"]))
files = os.listdir(
    "/".join([os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "greys1"]))
print(files)
data_rows = []
races = 0
false_races = 0
for file in files:
    MARKET = MARKETS[0]
    if ".csv" in file and "place" in file and "uk" in file:
        try:
            data = pd.read_csv(
                "/".join([os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "greys1", file]), header=0)
        except pd.errors.ParserError:
            continue
        data.sort_values('EVENT_ID', inplace=True)
        dfs = [g for i, g in data.groupby(
            data['EVENT_ID'].ne(data['EVENT_ID'].shift()).cumsum())]

        for df in dfs:
            races = races + 1
            # order in favourites
            row = {
                "trackName": df.iloc[0]['MENU_HINT'],
                "eventID": df.iloc[0]['EVENT_ID'],
                "numOfRunners": len(df.index),
                "tradedVol": df['PPTRADEDVOL'].sum()}

            df.sort_values('BSP', inplace=True)
            winners = 0

            print(len(df.index))
            for i in range(0, row['numOfRunners']):
                row.update({f'selectionId_{i+1}': df.iloc[i]['SELECTION_ID'],
                            f'odds_{i+1}': df.iloc[i]['BSP'], f'winner_{i+1}': True if df.iloc[i]['WIN_LOSE'] == 1 else False, f"weightedAP_{i+1}": df.iloc[i]['PPWAP'], f"amountTraded_{i+1}": (
                    100/row['tradedVol'])*df.iloc[i]['PPTRADEDVOL'], f'trap_{i+1}': df.iloc[i]['SELECTION_NAME']})
                # print(row)
                if df.iloc[i]['WIN_LOSE'] == 1:
                    winners = winners + 1

            # create flat json
            # if more than right winners delete:
            row['Winners'] = winners
            data_rows.append(row)

            print(row)


# run test
print(data_rows[0])
races = 0
bank = 100
bets = 0
staked = 0
unitSize = bank/100
GRAPH = {'x': [1], 'y': [bank]}
for event in data_rows:
    if "(AUS)" in event['trackName']:
        continue
    # needs book %
    races = races + 1
    book = 0
    pl = 0

    for i in range(event['Winners']+1, event['numOfRunners']):
        book = book + (100/event[f"odds_{i}"])
    print(event)
    print(book)
    if (event['Winners'] == 2 and book < 85) or (event['Winners'] == 3 and book < 140 and book > 120):
        # if (event['Winners'] == 2 and book < 85):
        # if (event['Winners'] == 3 and book < 140 and book > 120):
        # if (event['Winners'] == 4 and book < 220):
        bets = bets + 1
        for i in range(event['Winners']+1, event['numOfRunners']):
            stke = ((100/event[f"odds_{i}"])/book)*unitSize
            staked = staked + stke
            if event[f'winner_{i}'] == 1:
                pl = pl + (stke*(event[f"odds_{i}"]-1))
            else:
                pl = pl - stke

        bank = bank + pl
        GRAPH['x'].append(bets)
        GRAPH['y'].append(bank)
print(bank)
print(races)
print(bets)
print((100/staked)*(bank-100))

plt.figure(figsize=(10, 8))
plt.title('Scatter Plot', fontsize=20)
plt.xlabel('x', fontsize=15)
plt.ylabel('y', fontsize=15)
plt.scatter(GRAPH["x"], GRAPH["y"], marker='o')
plt.show()
